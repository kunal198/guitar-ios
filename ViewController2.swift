//
//  ViewController2.swift
//  politraker
//
//  Created by Brst on 6/26/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController2: UIViewController {

    @IBOutlet var lbl_legislator_name: UILabel!
    @IBOutlet var lbl_party_name: UILabel!
    @IBOutlet var lbl_district_name: UILabel!
    @IBOutlet var img_legislator: UIImageView!
    @IBOutlet var lbl_votes: UILabel!
    @IBOutlet var star1: UIImageView!
    @IBOutlet var star2: UIImageView!
    @IBOutlet var star3: UIImageView!
    @IBOutlet var star4: UIImageView!
    @IBOutlet var star5: UIImageView!
    
    var selected_legislator = String()
    var selected_district = String()
    var ratings = Int()
    var str1 = String()
    var checkStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //  checking for Legislator name
        if UserDefaults.standard.object(forKey: "item_selected_legislator") != nil
        {
            self.selected_legislator = UserDefaults.standard.object(forKey: "item_selected_legislator") as! String
        }

        // Getting  api data for Legislator
        if self.checkStr == "legislator" || self.checkStr == "district"
        {
            legislatorData .gettingData(withSuccess: { (result) in
                let dictData = result as NSMutableArray
                for index in 0..<dictData.count
                {
                    var dict = NSDictionary()
                    dict = dictData[index] as! NSDictionary
                    print(dict)
                    if self.selected_legislator == dict.value(forKey: "name_leg") as! String
                    {
                        self.str1 = (dict.value(forKey: "name_leg") as? String)!
                        self.lbl_legislator_name.text = self.str1.uppercased()//dict.value(forKey: "name_leg") as? String
                        let str12 = dict.value(forKey: "party_leg") as? String
                        self.lbl_party_name.text = str12?.uppercased()//dict.value(forKey: "party_leg") as? String
                        let str13 = dict.value(forKey: "name_dis") as? String
                        self.lbl_district_name.text = str13?.uppercased()//dict.value(forKey: "name_dis") as? String
                        self.ratings = (dict.value(forKey: "ratings") as? Int)!
                        
                        switch self.ratings
                        {
                            case 1:
                                self.star1.image = UIImage(named: "star")
                                self.star2.image = UIImage(named: "rating_star")
                                self.star3.image = UIImage(named: "rating_star")
                                self.star4.image = UIImage(named: "rating_star")
                                self.star5.image = UIImage(named: "rating_star")

                                break
                            case 2:
                                self.star1.image = UIImage(named: "star")
                                self.star2.image = UIImage(named: "star")
                                self.star3.image = UIImage(named: "rating_star")
                                self.star4.image = UIImage(named: "rating_star")
                                self.star5.image = UIImage(named: "rating_star")
                                
                                break
                            case 3:
                                self.star1.image = UIImage(named: "star")
                                self.star2.image = UIImage(named: "star")
                                self.star3.image = UIImage(named: "star")
                                self.star4.image = UIImage(named: "rating_star")
                                self.star5.image = UIImage(named: "rating_star")
                                break
                            case 4:
                                self.star1.image = UIImage(named: "star")
                                self.star2.image = UIImage(named: "star")
                                self.star3.image = UIImage(named: "star")
                                self.star4.image = UIImage(named: "star")
                                self.star5.image = UIImage(named: "rating_star")
                                break
                            case 5:
                                self.star1.image = UIImage(named: "star")
                                self.star2.image = UIImage(named: "star")
                                self.star3.image = UIImage(named: "star")
                                self.star4.image = UIImage(named: "star")
                                self.star5.image = UIImage(named: "star")
                                break
                            default:
                                self.star1.image = UIImage(named: "rating_star")
                                self.star2.image = UIImage(named: "rating_star")
                                self.star3.image = UIImage(named: "rating_star")
                                self.star4.image = UIImage(named: "rating_star")
                                self.star5.image = UIImage(named: "rating_star")

                                break
                            
                        }
                        let votes = dict.value(forKey: "votes") as! Int
                        self.lbl_votes.text = ("\(votes)\("")\(" VOTES")")
                        let img_url = dict.value(forKey: "img_url") as? String
                        
                        self.img_legislator.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "avtar"))
                    }
                
                }
            
                print(dictData)
            }, failure: { (error) in
                print (error)
                });
        }
        
        if self.checkStr == "bills"
        {
            // Getting  api data for Bills
            billsData .getting_Bills_Data(withSuccess: { (result) in
                
                let dictData = result
                for index in 0..<dictData.count
                {
                    var dict = NSDictionary()
                    dict = dictData[index] as! NSDictionary
                    print(dict)
                    
                    let valueArray = dict.value(forKey: "values") as! NSArray
                    print(valueArray)
                    
                    let valuesDict = valueArray[0] as! NSDictionary
                    print(valuesDict)

                   print("self.selected_legislator = \(String(describing: valuesDict.value(forKey: "name_leg") as? String))")
                    
                    if self.selected_legislator == valuesDict.value(forKey: "name_leg") as? String
                    {
                        print(dict)
                        self.str1 = (valuesDict.value(forKey: "name_leg") as? String)!
                        self.lbl_legislator_name.text =  self.str1.uppercased()//valuesDict.value(forKey: "name_leg") as? String
                        let str112 = valuesDict.value(forKey: "party_leg") as? String
                        self.lbl_party_name.text = str112?.uppercased()//valuesDict.value(forKey: "party_leg") as? String
                        let str113 = valuesDict.value(forKey: "district") as? String
                        self.lbl_district_name.text = str113?.uppercased()
                        self.ratings = (valuesDict.value(forKey: "ratings") as? Int)!
                        
                        switch self.ratings
                        {
                        case 1:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "rating_star")
                            self.star3.image = UIImage(named: "rating_star")
                            self.star4.image = UIImage(named: "rating_star")
                            self.star5.image = UIImage(named: "rating_star")
                            
                            break
                        case 2:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "star")
                            self.star3.image = UIImage(named: "rating_star")
                            self.star4.image = UIImage(named: "rating_star")
                            self.star5.image = UIImage(named: "rating_star")
                            
                            break
                        case 3:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "star")
                            self.star3.image = UIImage(named: "star")
                            self.star4.image = UIImage(named: "rating_star")
                            self.star5.image = UIImage(named: "rating_star")
                            break
                        case 4:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "star")
                            self.star3.image = UIImage(named: "star")
                            self.star4.image = UIImage(named: "star")
                            self.star5.image = UIImage(named: "rating_star")
                            break
                        case 5:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "star")
                            self.star3.image = UIImage(named: "star")
                            self.star4.image = UIImage(named: "star")
                            self.star5.image = UIImage(named: "star")
                            break
                        default:
                            self.star1.image = UIImage(named: "rating_star")
                            self.star2.image = UIImage(named: "rating_star")
                            self.star3.image = UIImage(named: "rating_star")
                            self.star4.image = UIImage(named: "rating_star")
                            self.star5.image = UIImage(named: "rating_star")
                            
                            break
                            
                        }
                        let votes = valuesDict.value(forKey: "votes") as! Int
                        self.lbl_votes.text = ("\(votes)\("")\(" votes")")
                        
                        //    self.lbl_district_name.text = dict.value(forKey: "name_dis") as? String
                        let img_url = valuesDict.value(forKey: "img_url") as? String
                      
                        self.img_legislator.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "avtar"))
                    }
                
                }
                
                print(dictData)
            }, failure: { (error) in
                print (error)
            });
        }
     /*   if self.checkStr == "district"
        {
            // Getting  api data for District
            districtData .getting_District_Data(withSuccess: { (result) in
                let dictData = result
                for index in 0..<dictData.count
                {
                    var dict = NSDictionary()
                    dict = dictData[index] as! NSDictionary
                    print(dict)
                    
                    let valueArray = dict.value(forKey: "legislators") as! NSArray
                    
                    let valuesDict = valueArray[0] as! NSDictionary
                    print(valuesDict)
                    
                    
                    if self.selected_legislator == valuesDict.value(forKey: "name_leg") as! String
                    {
                        self.str1 =  ((valuesDict.value(forKey: "name_leg") as? String)!)
                        self.lbl_legislator_name.text = self.str1.uppercased()        //valuesDict.value(forKey: "name_leg") as? String
                        let str1 = valuesDict.value(forKey: "party_leg") as? String
                        self.lbl_party_name.text = str1?.uppercased()             //valuesDict.value(forKey: "party_leg") as? String
                        let str113 = valuesDict.value(forKey: "district_name") as? String
                        self.lbl_district_name.text = str113?.uppercased()
                        self.ratings = (valuesDict.value(forKey: "ratings") as? Int)!
                        
                        switch self.ratings
                        {
                        case 1:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "rating_star")
                            self.star3.image = UIImage(named: "rating_star")
                            self.star4.image = UIImage(named: "rating_star")
                            self.star5.image = UIImage(named: "rating_star")
                            
                            break
                        case 2:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "star")
                            self.star3.image = UIImage(named: "rating_star")
                            self.star4.image = UIImage(named: "rating_star")
                            self.star5.image = UIImage(named: "rating_star")
                            
                            break
                        case 3:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "star")
                            self.star3.image = UIImage(named: "star")
                            self.star4.image = UIImage(named: "rating_star")
                            self.star5.image = UIImage(named: "rating_star")
                            break
                        case 4:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "star")
                            self.star3.image = UIImage(named: "star")
                            self.star4.image = UIImage(named: "star")
                            self.star5.image = UIImage(named: "rating_star")
                            break
                        case 5:
                            self.star1.image = UIImage(named: "star")
                            self.star2.image = UIImage(named: "star")
                            self.star3.image = UIImage(named: "star")
                            self.star4.image = UIImage(named: "star")
                            self.star5.image = UIImage(named: "star")
                            break
                        default:
                            self.star1.image = UIImage(named: "rating_star")
                            self.star2.image = UIImage(named: "rating_star")
                            self.star3.image = UIImage(named: "rating_star")
                            self.star4.image = UIImage(named: "rating_star")
                            self.star5.image = UIImage(named: "rating_star")
                            
                            break
                            
                        }
                        let votes = valuesDict.value(forKey: "votes") as! Int
                        self.lbl_votes.text = ("\(votes)\("")\(" VOTES")")
                        
                        //    self.lbl_district_name.text = dict.value(forKey: "name_dis") as? String
                        let img_url = valuesDict.value(forKey: "img_url") as? String
                        
                        self.img_legislator.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "avtar"))
                    }
                    
                }
                
              //  print(dictData)
            }, failure: { (error) in
                print (error)
            });
        }
    
        */
                
                
      /*
                let dictData = result as NSMutableArray
                for index in 0..<dictData.count
                {
                    var dict = NSDictionary()
                    dict = dictData[index] as! NSDictionary
                    print(dict)
                    let data = UserDefaults.standard.object(forKey: "item_selected_district") as! String
                    if data == dict.value(forKey: "district_name") as! String
                    {
                        let dictData1 = dict.value(forKey: "legislators") as! NSMutableArray
                        print(dictData1)
                        for index1 in 0..<dictData1.count
                        {
                            var dict1 = NSDictionary()
                            dict1 = dictData1[index1] as! NSDictionary
                            if self.selected_district == dict1.value(forKey: "name_leg") as? String
                            {
                                self.lbl_legislator_name.text = dict1.value(forKey: "name_leg") as? String
                                self.lbl_party_name.text = dict1.value(forKey: "party_leg") as? String
                                self.lbl_district_name.text = UserDefaults.standard.object(forKey: "item_selected_district") as? String
                                let img_url = dict1.value(forKey: "img_url") as? String
                                self.img_legislator.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "avtar"))
                            }
                        }
                    }
                }
                
                print(dictData)
            }, failure: { (error) in
                print (error)
            });
        }

        */
        

        
        
        self.img_legislator.layer.cornerRadius = self.img_legislator.frame.size.width/2
        self.img_legislator.clipsToBounds = true
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_bills_clicked(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        UserDefaults.standard.set(self.str1, forKey: "item_selected_legislator")

        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "except_district") as! ViewController_for_district
        nextViewController.table_title = "BILLS & VOTES"
        nextViewController.checkCategory = "bills"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func btn_categories_clicked(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        UserDefaults.standard.set(self.str1, forKey: "item_selected_legislator")

        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "except_district") as! ViewController_for_district
        nextViewController.table_title = "CATEGORIES & VOTES"
        nextViewController.checkCategory = "category"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

    @IBAction func homeAction(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
   
    @IBAction func btn_go_back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
      //  self.navigationController?.popToRootViewController(animated: true)

//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
//        nextViewController.selected_bills =  ""       //self.lbl_legislator_name.text!
//        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
}
