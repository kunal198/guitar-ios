//
//  ViewController_election2018_2.swift
//  politraker
//
//  Created by brst on 28/06/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import SDWebImage
import AFNetworking

class ViewController_election2018_2: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var btn_menu_title: UIButton!
    @IBOutlet weak var lbl_menu_title: UILabel!
    @IBOutlet weak var view_menu_btn: UIView! //
    @IBOutlet weak var view_aboutUs: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet var view2: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var view_johnDoe: UIView!
    @IBOutlet weak var view_searchByCandidate: UIView!
    @IBOutlet weak var tableView1: UITableView! //
    @IBOutlet var lbl_educationOrCandidate: UILabel!
    
    @IBOutlet var img_legislator: UIImageView!
    @IBOutlet var lbl_legislator_name: UILabel!
    @IBOutlet var lbl_party_name: UILabel!
    @IBOutlet var lbl_district_name: UILabel!
    @IBOutlet var lbl_legislator_list: UILabel!
    
    @IBOutlet var btn_undecided: UIButton!
    @IBOutlet var btn_no: UIButton!
    @IBOutlet var btn_yes: UIButton!
    var selected_legislator = String()
    var checkStr = String()
    var didSelect = Bool()
    var legislatorArray = NSMutableArray()
    var partyArray = NSMutableArray()
    var districtArray = NSMutableArray()
    var imgArray = NSMutableArray()

    var legislatorArray1 = NSMutableArray()
    var billsArray1 = NSMutableArray()
    var districtArray1 = NSMutableArray()
    var categoryArray1 = NSMutableArray()
    
    var selected_legislator1 = String()
    var selected_bills1 = String()
    var selected_district1 = String()
    var selected_category1 = String()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view2.isHidden = true

        // Getting data- Legislator List
        if UserDefaults.standard.object(forKey: "item_selected_legislator") != nil
        {
            self.selected_legislator = UserDefaults.standard.object(forKey: "item_selected_legislator") as! String
        }
        legislatorData .gettingData(withSuccess: { (result) in
            let dictData = result as NSMutableArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                self.legislatorArray.add(dict.value(forKey: "name_leg") as! String)
                self.partyArray.add(dict.value(forKey: "party_leg") as! String)
                self.imgArray.add(dict.value(forKey: "img_url") as! String)
                self.districtArray.add(dict.value(forKey: "name_dis") as! String)
                
                if self.legislatorArray1.contains(dict.value(forKey: "name_leg") as! String)
                {   }
                else
                {
                    self.legislatorArray1.add(dict.value(forKey: "name_leg") as! String)
                }

            }
            
            self.tableView.reloadData()
            }, failure: { (error) in
            print (error)
            });

        
        // Getting  api data for Bills
        billsData .getting_Bills_Data(withSuccess: { (result) in
            print (result)
            let dictData = result
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.billsArray1.contains(dict.value(forKey: "bill_name") as! String)
                {   }
                else
                {
                    self.billsArray1.add(dict.value(forKey: "bill_name") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });
        
        // Getting  api data for District
        districtData .getting_District_Data(withSuccess: { (result) in
            print (result)
            let dictData = result as NSMutableArray
            print(dictData)
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.districtArray1.contains(dict.value(forKey: "district_name") as! String)
                {   }
                else
                {
                    self.districtArray1.add(dict.value(forKey: "district_name") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });
        
        // Getting  api data for Category
        categoryData .getting_Category_Data(withSuccess: { (result) in
            print (result)
            let dictData = result as NSMutableArray
            print(dictData)
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.categoryArray1.contains(dict.value(forKey: "cat_name") as! String)
                {   }
                else
                {
                    self.categoryArray1.add(dict.value(forKey: "cat_name") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });
        

        
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view1.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view1.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view1.addSubview(blurEffectView)
        }
        
        self.btn_menu_title.layer.cornerRadius = 5
        self.btn_menu_title.layer.borderWidth = 1
        self.btn_menu_title.layer.borderColor = UIColor.gray.cgColor
        
        // view board style :
        self.view_johnDoe.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_johnDoe.layer.shadowOpacity = 0.5
        self.view_johnDoe.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_johnDoe.layer.shadowRadius = 10
        self.view_johnDoe.layer.cornerRadius = 20
        self.view_johnDoe.layer.masksToBounds = false
        self.view_johnDoe.layer.shadowPath = UIBezierPath(rect: self.view_johnDoe.bounds).cgPath
        self.view_johnDoe.layer.shouldRasterize = true
        self.view_johnDoe.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_aboutUs.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_aboutUs.layer.shadowOpacity = 0.5
        self.view_aboutUs.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_aboutUs.layer.shadowRadius = 10
        self.view_aboutUs.layer.cornerRadius = 20
        self.view_aboutUs.layer.masksToBounds = false
        self.view_aboutUs.layer.shadowPath = UIBezierPath(rect: self.view_johnDoe.bounds).cgPath
        self.view_aboutUs.layer.shouldRasterize = true
        self.view_aboutUs.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_menu_btn.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_menu_btn.layer.shadowOpacity = 0.5
        self.view_menu_btn.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_menu_btn.layer.shadowRadius = 10
        self.view_menu_btn.layer.cornerRadius = 20
        self.view_menu_btn.layer.masksToBounds = false
        self.view2.layer.masksToBounds = false  // view2
        self.view_menu_btn.layer.shadowPath = UIBezierPath(rect: self.view_menu_btn.bounds).cgPath
        self.view_menu_btn.layer.shouldRasterize = true
        self.view_menu_btn.layer.rasterizationScale =  UIScreen.main.scale
    }
    override func viewWillAppear(_ animated: Bool)
    {
            if checkStr == "issues"
            {
                self.lbl_educationOrCandidate.text = "EDUCATION & WORKFORCE ISSUES"
                self.view_searchByCandidate.isHidden = true
            }
            if checkStr == "candidates"
            {
                self.lbl_educationOrCandidate.text = "SEARCH BY CANDIDATES"
                self.view_searchByCandidate.isHidden = false
            }
            if checkStr == "candidacy"
            {
                self.lbl_educationOrCandidate.text = "CANDIDATE & RUNEMATE"
                self.view_searchByCandidate.isHidden = true
            }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: Table view Function
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView1
        {
            if self.lbl_menu_title.text == "CHOOSE LEGISLATOR"
            {
                return self.legislatorArray1.count
            }
            if self.lbl_menu_title.text == "CHOOSE THE BILLS"
            {
                return self.billsArray1.count
            }
            if self.lbl_menu_title.text == "CHOOSE CATEGORY"
            {
                return self.categoryArray1.count
            }
            if self.lbl_menu_title.text == "CHOOSE DISTRICT"
            {
                return self.districtArray1.count
            }
            
        }
        return self.legislatorArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableView1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell

            if self.lbl_menu_title.text == "CHOOSE LEGISLATOR"
            {
                cell.lbl.text = self.legislatorArray1[indexPath.row] as? String
                return cell
                
            }
            if self.lbl_menu_title.text == "CHOOSE THE BILLS"
            {
                cell.lbl.text = self.billsArray1[indexPath.row] as? String
                return cell
                
            }
            if self.lbl_menu_title.text == "CHOOSE CATEGORY"
            {
                cell.lbl.text = self.categoryArray1[indexPath.row] as? String
                return cell
                
            }
            if self.lbl_menu_title.text == "CHOOSE DISTRICT"
            {
                cell.lbl.text = self.districtArray1[indexPath.row] as? String
                return cell
                
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell_GoBACK_2
        
        let str = self.legislatorArray[indexPath.row] as? String
        cell.lbl_legislator_name.text = str?.uppercased()
        let str1 = self.partyArray[indexPath.row] as? String
        cell.lbl_party_name.text = str1?.uppercased()
        let str2 = self.districtArray[indexPath.row] as? String
        cell.lbl_district_name.text = str2?.uppercased()
        
        let img_url = self.imgArray[indexPath.row]
        
        cell.img_legislator.sd_setImage(with: URL(string: img_url as! String), placeholderImage: UIImage(named: "avtar"))
        cell.img_legislator.layer.cornerRadius = cell.img_legislator.frame.size.width/2
        cell.img_legislator.clipsToBounds = true
        cell.img_legislator.frame.size.height = cell.img_legislator.frame.size.width
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableView1
        {
            if self.lbl_menu_title.text == "CHOOSE LEGISLATOR"
            {
                self.lbl_legislator_list.text = self.legislatorArray[indexPath.row] as? String
                self.selected_legislator1 = (self.legislatorArray[indexPath.row] as? String)!
                self.tableView1.isHidden = true
            }
            if self.lbl_menu_title.text == "CHOOSE THE BILLS"
            {
                self.lbl_legislator_list.text = self.billsArray1[indexPath.row] as? String
                self.selected_bills1 = (self.billsArray1[indexPath.row] as? String)!
                self.tableView1.isHidden = true
            }
            if self.lbl_menu_title.text == "CHOOSE DISTRICT"
            {
                self.lbl_legislator_list.text = self.districtArray1[indexPath.row] as? String
                self.selected_district1 = (self.districtArray1[indexPath.row] as? String)!
                self.tableView1.isHidden = true
            }
            if self.lbl_menu_title.text == "CHOOSE CATEGORY"
            {
                self.lbl_legislator_list.text = self.categoryArray1[indexPath.row] as? String
                self.selected_category1 = (self.categoryArray1[indexPath.row] as? String)!
                self.tableView1.isHidden = true
            }
           
        }
        else if tableView == tableView
        {
            self.didSelect = true
            self.lbl_educationOrCandidate.text = "SEARCH BY CANDIDATES"
            self.lbl_legislator_name.text = self.legislatorArray[indexPath.row] as? String
            self.lbl_district_name.text = self.districtArray[indexPath.row] as? String
            self.lbl_party_name.text = self.partyArray[indexPath.row] as? String
            let imgUrl = self.imgArray[indexPath.row]
            
            self.img_legislator.sd_setImage(with: URL(string: imgUrl as! String), placeholderImage: UIImage(named: "avtar"))
            self.img_legislator.layer.cornerRadius = self.img_legislator.frame.size.width/2
            self.img_legislator.clipsToBounds = true
            self.view_searchByCandidate.isHidden = false
        }
    }
    @IBAction func btn_education(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view_aboutUs.isHidden = false
    }
    @IBAction func btn_aboutUs_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.view_aboutUs.isHidden = true
    }
    @IBAction func btn_menu_submit(_ sender: Any)
    {
        if self.lbl_menu_title.text == "CHOOSE LEGISLATOR"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2") as! ViewController2
            nextViewController.selected_legislator = self.selected_legislator1   //  $$$$$
            nextViewController.checkStr = "legislator"
            UserDefaults.standard.removeObject(forKey: "viewController2_subview")
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_legislator")

            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //
        if self.lbl_menu_title.text == "CHOOSE THE BILLS"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_bills")

            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.strBtnValue = "chooseBills"
            nextViewController.selected_bills = self.selected_bills1
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        if self.lbl_menu_title.text == "CHOOSE DISTRICT"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_district")

            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.selected_district = self.selected_district1
            nextViewController.strBtnValue = "chooseDistrict"
            nextViewController.strCategory = true
            nextViewController.strDistrict = true
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        if self.lbl_menu_title.text == "CHOOSE CATEGORY"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_category")

            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
           // nextViewController.selected_category = self.selected_category1
            nextViewController.strBtnValue = "chooseCategory"
            nextViewController.strCategory = true
            nextViewController.strDistrict = false
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
       }
    @IBAction func btn_category(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_menu_btn.isHidden = false
        self.lbl_menu_title.text = "CHOOSE CATEGORY"
        self.lbl_legislator_list.text = self.categoryArray1[0] as? String
       // self.selected_category1 = (self.categoryArray1[0] as? String)!
       // self.btn_menu_title .setTitle(   self.categoryArray1[0] as? String , for: UIControlState.normal)
        self.tableView1.reloadData()
    }
    @IBAction func btn_bills(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_menu_btn.isHidden = false
        self.lbl_menu_title.text = "CHOOSE THE BILLS"
        self.lbl_legislator_list.text = self.billsArray1[0] as? String
       // self.selected_bills1 = (self.billsArray1[0] as? String)!

       // self.btn_menu_title .setTitle(   self.billsArray1[0] as? String , for: UIControlState.normal)
        self.tableView1.reloadData()
    }
    @IBAction func btn_district(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_menu_btn.isHidden = false
        self.lbl_menu_title.text = "CHOOSE DISTRICT"
        self.lbl_legislator_list.text = self.districtArray1[0] as? String
       // self.selected_legislator1 = (self.districtArray1[0] as? String)!

       // self.btn_menu_title .setTitle(   self.districtArray1[0] as? String , for: UIControlState.normal)
        self.tableView1.reloadData()
    }
    @IBAction func btn_legislator(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_menu_btn.isHidden = false
        self.lbl_menu_title.text = "CHOOSE LEGISLATOR"
        self.lbl_legislator_list.text = self.legislatorArray1[0] as? String
      //  self.selected_legislator1 = (self.legislatorArray1[0] as? String)!

       // self.btn_menu_title .setTitle(   self.legislatorArray[0] as? String, for: UIControlState.normal)
        self.tableView1.reloadData()
    }
    @IBAction func btn_menu_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.view2.isHidden = true
        self.view_menu_btn.isHidden = true
    }
    @IBAction func btn_menu_title_selected(_ sender: Any)
    {
        if self.tableView1.isHidden == false
        {
            self.tableView1.isHidden = true
        }
        else
        {
            self.tableView1.isHidden = false
        }
       // self.tableView1.isHidden = false
    }
    @IBAction func btn_goBack(_ sender: Any)
    {
        if didSelect == true
        {
            didSelect = false
            if checkStr == "issues"
            {
                self.lbl_educationOrCandidate.text = "EDUCATION & WORKFORCE ISSUES"
                self.view_searchByCandidate.isHidden = true
            }
            if checkStr == "candidacy"
            {
                self.lbl_educationOrCandidate.text = "CANDIDATE & RUNEMATE"
                self.view_searchByCandidate.isHidden = true
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func homeAction(_ sender: Any)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func btn_yes_clicked(_ sender: Any)
    {
        self.btn_yes.setImage(UIImage(named:"green_s_copy_20"), for: UIControlState.normal)
        self.btn_no.setImage(UIImage(named:"redb"), for: UIControlState.normal)
        self.btn_undecided.setImage(UIImage(named: "black_copy_20"), for: UIControlState.normal)
    }
    @IBAction func btn_no_clicked(_ sender: Any)
    {
        self.btn_no.setImage(UIImage(named:"red-s_copy_20"), for: UIControlState.normal)
        self.btn_yes.setImage(UIImage(named:"green_copy_20"), for: UIControlState.normal)
        self.btn_undecided.setImage(UIImage(named: "black_copy_20"), for: UIControlState.normal)
    }
    @IBAction func btn_undecided_clicked(_ sender: Any)
    {
        self.btn_undecided.setImage(UIImage(named:"black_s_copy_20"), for: UIControlState.normal)
        self.btn_no.setImage(UIImage(named:"redb"), for: UIControlState.normal)
        self.btn_yes.setImage(UIImage(named: "green_copy_20"), for: UIControlState.normal)
    }
  
}
