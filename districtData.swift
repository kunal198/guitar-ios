//
//  districtData.swift
//  politraker
//
//  Created by Brst on 7/12/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import AFNetworking

class districtData: NSObject {
    
    class  func getting_District_Data( withSuccess success: @escaping ( _ dic: NSMutableArray) -> Void, failure: @escaping ( _ err: String) -> Void)
        
    {
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        
        manager.get(
            "http://www.politrackervi.com/apis/api.php?api=district",
            parameters: nil, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    let dataDict = jsonDict?.object(forKey: "data") as! NSMutableArray
                    print("dataDict = \(String(describing: dataDict))")
                    
                    success(dataDict)
                }catch{
                    
                }
                
        },
            failure:
            {
                (operation, error) in
                failure(error as! String)
                print(error)
                print("Error: " + error.localizedDescription)
        })
    }


}
