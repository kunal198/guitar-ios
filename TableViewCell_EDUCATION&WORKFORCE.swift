//
//  TableViewCell_EDUCATION&WORKFORCE.swift
//  politraker
//
//  Created by brst on 27/06/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class TableViewCell_EDUCATION_WORKFORCE: UITableViewCell {

    @IBOutlet weak var lbl_title: UILabel!
   
    @IBOutlet var view_category: UIView!
    @IBOutlet var lbl_sponsor: UILabel!
    @IBOutlet var lbl_voted: UILabel!
    @IBOutlet var img_sponsor: UIImageView!
    @IBOutlet var img_votes: UIImageView!
    
    @IBOutlet var lbl_votes_yes: UILabel!
    @IBOutlet var lbl_votes_no: UILabel!
    @IBOutlet var lbl_sponsor_count: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
