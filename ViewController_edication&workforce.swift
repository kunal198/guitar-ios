//
//  ViewController_edication&workforce.swift
//  politraker
//
//  Created by brst on 27/06/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController_edication_workforce: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {

    @IBOutlet weak var view_casteYourVote: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var BILL_SUMMARY: UIView!
    @IBOutlet weak var view_userSignIn: UIView!
    @IBOutlet weak var VIEW_RATING: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view_userName: UIView!
    @IBOutlet weak var view_email: UIView!
    @IBOutlet var lbl_billSummary_titleStr: UILabel!
    @IBOutlet var txtView_billsummary_details: UITextView!
    
    @IBOutlet weak var view_bills_selection: UIView!
    @IBOutlet var view_menu_11: UIView!
    @IBOutlet weak var lbl_billsOrCategory: UILabel!
    @IBOutlet weak var btn_table_title: UIButton!
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet var lbl_title_str: UILabel!
    
    @IBOutlet var img_legislator: UIImageView!
    @IBOutlet var lbl_legislator_name: UILabel!
    @IBOutlet var lbl_party_name: UILabel!
    @IBOutlet var lbl_district_name: UILabel!
    @IBOutlet var star1: UIImageView!
    @IBOutlet var star2: UIImageView!
    @IBOutlet var star3: UIImageView!
    @IBOutlet var star4: UIImageView!
    @IBOutlet var star5: UIImageView!
    @IBOutlet var lbl_votes: UILabel!
    @IBOutlet var lbl_votes_yes: UILabel!
    @IBOutlet var lbl_votes_no: UILabel!
    @IBOutlet var lbl_sponsor: UILabel!
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var lbl_tabletitle_str: UILabel!
    
    
    var data = NSMutableArray()
    var table_title = String()
    var checkTableTitle = Bool()
    var selected_legislator = String()
    var ratings = Int()
    var leg_id = String()
    var array_category = NSMutableArray()
    var array_categorySummary = NSMutableArray()
    var array_bills = NSMutableArray()
    var array_billsSummary = NSMutableArray()
    var urlStr = NSMutableArray()
    var index1 = Int()
    var billsArray = NSMutableArray()
    var categoryArray = NSMutableArray()
    var checkBillsBtn = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getting_legislatorDetails()

        self.indicator.isHidden = false
        if UserDefaults.standard.object(forKey: "item_selected_legislator") != nil
        {
            self.selected_legislator = UserDefaults.standard.object(forKey: "item_selected_legislator") as! String
        }
        

        self.img_legislator.layer.cornerRadius = self.img_legislator.frame.size.width/2
        self.img_legislator.clipsToBounds = true
        
        
        data = ["H.R.1628: AMERICAN HEALTH CARE ACT OF 2017","S.1553: PAIN CAPABLE UNBORN CHILD PROTECTION ACT","H.R.2: MEDICARE ACCESS AND CHIP REAUTHRIZATION ACT","H.R.1628: AMERICAN HEALTH CARE ACT OF 2017","S.1553: PAIN CAPABLE UNBORN CHILD PROTECTION ACT","H.R.2: MEDICARE ACCESS AND CHIP REAUTHRIZATION ACT"]
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view1.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view1.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view1.addSubview(blurEffectView)
        }

        self.ViewLayout()
      
        
        
    }
    //MARK: View Will Appear
    override func viewWillAppear(_ animated: Bool)
    {
       // self.indicator.isHidden = false
       // self.getting_legislatorDetails()
        self.BillsAndCategory()
      //  self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Table view Function 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView1
        {
            if self.checkBillsBtn == true
            {
                return self.billsArray.count
            }
            else
            {
                return self.categoryArray.count
            }
            
        }
        return self.array_category.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if tableView == tableView1
        {
            let cell1 = tableView1.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCell_BillsOrCategory
            if self.checkBillsBtn == true
            {
                cell1.lbl.text = self.billsArray[indexPath.row] as? String
                return cell1
            }
            else
            {
                cell1.lbl.text = self.categoryArray[indexPath.row] as? String
                return cell1
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell_EDUCATION_WORKFORCE
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: self.array_category[indexPath.row] as! String, attributes: underlineAttribute)
    
        cell.lbl_title.attributedText = underlineAttributedString
//        cell.lbl_votes_yes.text = self.array_votes_yes[indexPath.row] as? String
//        cell.lbl_votes_no.text = self.array_votes_no[indexPath.row] as? String
    
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tableView1
        {
            if self.checkBillsBtn == true
            {
                self.lbl_tabletitle_str.text = self.billsArray[indexPath.row] as?  String
                UserDefaults.standard.set(self.billsArray[indexPath.row], forKey: "selected_btn")
                self.tableView1.isHidden = true
            }
            else
            {
                
                self.lbl_tabletitle_str.text = self.categoryArray[indexPath.row] as? String
                UserDefaults.standard.set(self.categoryArray[indexPath.row], forKey: "selected_btn")
                self.tableView1.isHidden = true
            }
        }
        else
        {
            self.index1 = indexPath.row
            self.BILL_SUMMARY.isHidden = false
            self.index1 = indexPath.row
            self.lbl_billSummary_titleStr.text = self.array_category[indexPath.row] as? String
            let summary = self.array_billsSummary[indexPath.row] as? String
            if (summary == "0")
            {
                self.txtView_billsummary_details.text = ""
            }
            else
            {
                self.txtView_billsummary_details.text = summary
                self.view1.isHidden = false
            }
        }

    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        tableView.rowHeight = UITableViewAutomaticDimension
//        tableView.estimatedRowHeight = 140
//        return 82
//    }
    @IBAction func btn_exit(_ sender: Any)
    {
        self.BILL_SUMMARY.isHidden = true
        self.view1.isHidden = true

    }
    @IBAction func casteYourVote(_ sender: Any)
    {
        if UserDefaults.standard.object(forKey: "loginCheck") != nil
        {
            let loginCheck = UserDefaults.standard.object(forKey: "loginCheck") as? Bool
            if loginCheck == true
            {
                self.VIEW_RATING.isHidden = false
                self.view_userSignIn.isHidden = true
                self.view1.isHidden = false
            }
            else
            {
                self.view_userSignIn.isHidden = false
                 self.view1.isHidden = false
            }
        }
        else
        {
           // self.view_casteYourVote.isHidden = true
            self.view_userSignIn.isHidden = false
             self.view1.isHidden = false
        }
        
    }
    @IBAction func userSignIn_exit(_ sender: Any)
    {
        self.view_userSignIn.isHidden = true
        self.view1.isHidden = true

    }
    @IBAction func btn_userSignIn(_ sender: Any)
    {
        self.VIEW_RATING.isHidden = false
        self.view_userSignIn.isHidden = true
        self.view1.isHidden = false
    }
    @IBAction func btn_rating_exit(_ sender: Any)
    {
        self.VIEW_RATING.isHidden = true
        self.view1.isHidden = true
    }
    @IBAction func btn_GoBack(_ sender: Any)
    {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 2], animated: true);
    }
    
    @IBAction func btn_table_title_clicked(_ sender: Any)
    {
        self.tableView1.isHidden = false
    }
    
    @IBAction func btn_billsOrCategory_submit(_ sender: Any)
    {
        if self.lbl_billsOrCategory.text == "CHOOSE THE BILL"
        {
//            UserDefaults.standard.set(self.lbl_tabletitle_str.text, forKey: "item_selected_bills")
//            
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
//            nextViewController.strBtnValue = "chooseBills"
//            self.navigationController?.pushViewController(nextViewController, animated: true)

            UserDefaults.standard.set(self.lbl_tabletitle_str.text, forKey: "item_selected_bills")
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.strBtnValue = "chooseBills"
            nextViewController.selected_bills = self.lbl_tabletitle_str.text!    //self.selected_bills
            self.navigationController?.pushViewController(nextViewController, animated: true)
        
        }
        if self.lbl_billsOrCategory.text == "CHOOSE CATEGORY"
        {
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
//            nextViewController.strBtnValue = "chooseCategory"
//            self.navigationController?.pushViewController(nextViewController, animated: true)
            
            UserDefaults.standard.set(self.lbl_tabletitle_str.text , forKey: "item_selected_category")
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.selected_category = self.lbl_tabletitle_str.text!   //self.selected_category
            nextViewController.strBtnValue = "chooseCategory"
            nextViewController.strCategory = true
            nextViewController.strDistrict = false
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
    }
    //MARK: Bills Clicked
    @IBAction func btn_bills_clicked(_ sender: Any)
    {
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "except_district") as! ViewController_for_district
//        nextViewController.table_title = "BILLS & VOTES"
//        nextViewController.checkCategory = "bills"
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        self.checkBillsBtn = true
        self.view1.isHidden = false
        self.view_bills_selection.isHidden = false
        self.lbl_billsOrCategory.text = "CHOOSE THE BILL"
        self.lbl_tabletitle_str.text = self.billsArray[0] as? String
        self.tableView1.reloadData()

       //  self.btn_table_title .setTitle(self.billsArray[0] as? String, for: UIControlState.normal)
    }
    @IBAction func btn_bills_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.view_bills_selection.isHidden = true
    }
    @IBAction func btn_category_clicked(_ sender: Any)
    {
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "except_district") as! ViewController_for_district
//        nextViewController.table_title = "CATEGORIES & VOTES"
//        nextViewController.checkCategory = "category"
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        self.checkBillsBtn = false

        self.view1.isHidden = false
        self.view_bills_selection.isHidden = false
        self.lbl_billsOrCategory.text = "CHOOSE CATEGORY"
        self.lbl_tabletitle_str.text = self.categoryArray[0] as? String
        self.tableView1.reloadData()


       // self.btn_table_title .setTitle(self.categoryArray[0] as? String, for: UIControlState.normal)
    }
    @IBAction func homeAction(_ sender: Any)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: Getting Legislator details
    func getting_legislatorDetails()
    {
        // Getting  api data for Legislator
        legislatorData .gettingData(withSuccess: { (result) in
            let dictData = result as NSMutableArray
            print(dictData)
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
               // print(dict)
                if self.selected_legislator == dict.value(forKey: "name_leg") as! String
                {
                    let str1 = dict.value(forKey: "name_leg") as? String
                    self.lbl_legislator_name.text = str1?.uppercased()   //dict.value(forKey: "name_leg") as? String
                    let str2 = dict.value(forKey: "party_leg") as? String
                    self.lbl_party_name.text = str2?.uppercased()  //dict.value(forKey: "party_leg") as? String
                    let str3 = dict.value(forKey: "name_dis") as? String
                    self.lbl_district_name.text =  str3?.uppercased()//dict.value(forKey: "name_dis") as? String
                    
                    self.leg_id = dict.value(forKey: "id_leg") as! String
                    self.ratings = (dict.value(forKey: "ratings") as? Int)!
                    
                    switch self.ratings
                    {
                    case 1:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "rating_star")
                        self.star3.image = UIImage(named: "rating_star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        
                        break
                    case 2:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "rating_star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        
                        break
                    case 3:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        break
                    case 4:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "star")
                        self.star4.image = UIImage(named: "star")
                        self.star5.image = UIImage(named: "rating_star")
                        break
                    case 5:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "star")
                        self.star4.image = UIImage(named: "star")
                        self.star5.image = UIImage(named: "star")
                        break
                    default:
                        self.star1.image = UIImage(named: "rating_star")
                        self.star2.image = UIImage(named: "rating_star")
                        self.star3.image = UIImage(named: "rating_star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        
                        break
                        
                    }
                    let votes = dict.value(forKey: "votes") as! Int
                    self.lbl_votes.text = ("\(votes)\("")\(" VOTES")")
                    
                    let img_url = dict.value(forKey: "img_url") as? String
                    
                    self.img_legislator.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "avtar"))
                    
                    self.getting_Category()
                    self.getting_Bills11()
                    DispatchQueue.main.async {
                        
                        self.tableView.reloadData()
                    }
                    
                }
                
            }
            
            print(dictData)
        }, failure: { (error) in

            print (error)
        });

    }
    
    //MARK: Getting Categories
    func getting_Category()
    {
        
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=legislators_cat")!)
        request.httpMethod = "POST"
        
        let postString = "leg_id=\(leg_id)"
        request.httpBody = postString.data(using: .utf8)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
     

        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            guard let data = data, error == nil else {
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            if((json) != nil){
                let data = json?.value(forKey: "data") as!NSArray
                print(data)
                if  data.value(forKey: "values") is [NSArray]
                {
                    DispatchQueue.main.async {
                        
                    let resultarr = data.value(forKey: "values") as! NSArray
                    let resultarr2 = resultarr[0] as! NSArray
                    
                    let str11 = data.value(forKey: "count") as! NSArray
                    print(str11)
                    let data11 = str11[0]
                    self.lbl_sponsor.text = ("Sponsor: \(data11)")
                   // }
                    for i in resultarr2
                    {
                       // DispatchQueue.main.async {
                            
                            let indexvalue = i as! NSDictionary
                        print(indexvalue)
                            self.array_category.add(indexvalue.value(forKey: "name_leb") as! String)
                            
                            self.lbl_votes_yes.text = indexvalue.value(forKey: "totalyea_leb") as? String
                            self.lbl_votes_no.text = indexvalue.value(forKey: "totalnay_leb") as? String
                            let selectionStr = UserDefaults.standard.object(forKey: "category_selected") as? String
                            self.lbl_title_str.text = selectionStr?.uppercased()
                            self.indicator.isHidden = true
                        }
                       
                    }

                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }

                }
                else
                {
                    self.indicator.isHidden = true
                }
            }
            else
            {
                self.indicator.isHidden = true
            }
        }
        task.resume()
    }
    //MARK: Getting Bills
    func getting_Bills11()
    {
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=legislators_bill")!)
        request.httpMethod = "POST"
        
        let postString = "leg_id=\(leg_id)"
        request.httpBody = postString.data(using: .utf8)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
          //  print(json)
            if((json?.value(forKey: "data")) != nil){
                let data = json?.value(forKey: "data") as!NSArray
              //  print(data)
                
                if  data.value(forKey: "values") is [NSArray]
                {
                    let resultarr = data.value(forKey: "values") as! NSArray
                    let resultarr2 = resultarr[0] as! NSArray
                  //  print(resultarr2)
                    for i in resultarr2
                    {
                        
                        let indexvalue = i as! NSDictionary
                        self.array_bills.add(indexvalue.value(forKey: "name_leb") as! String)
                        
                        
                        if  let val = indexvalue["summary_leb"] as? String {
                            print(val)
                         
                                self.array_billsSummary.add(indexvalue.value(forKey: "summary_leb") as! String)
                                self.urlStr.add(indexvalue.value(forKey: "url") as! String)
                          
                        } else
                        {
                            self.array_billsSummary.add("0")
                            self.urlStr.add(indexvalue.value(forKey: "url") as! String)

                           // print("nill")
                        }
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        
                    }
                    
                   // print(self.urlStr)
                  //  print(self.array_billsSummary)
                   // print(data)

                }
                else
                {
                    //
                }
                
            }
        }
        task.resume()
        
    }

    //MARK: getting Bills & Categories
    func BillsAndCategory()
    {
        billsData .getting_Bills_Data(withSuccess: { (result) in
          //  print (result)
            let dictData = result as NSArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
              //  print(dict)
               // UserDefaults.standard.set(dict.value(forKey: "bill_name") as! String, forKey: "selected_btn")
                if self.billsArray.contains(dict.value(forKey: "bill_name") as! String)
                {   }
                else
                {
                    self.billsArray.add(dict.value(forKey: "bill_name") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });
        
        
        // Getting  api data for Category
        categoryData .getting_Category_Data(withSuccess: { (result) in
              // print (result)
            let dictData = result as NSMutableArray
            //  print(dictData)
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                if self.categoryArray.contains(dict.value(forKey: "cat_name") as! String)
                {   }
                else
                {
                    self.categoryArray.add(dict.value(forKey: "cat_name") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });
        
    }
    
    
    @IBAction func btn_BillSummary_selected(_ sender: Any)
    {
        if self.urlStr.count > 0
        {
            let str = self.urlStr[index1] as! String
            let url = URL(string: str)  //URL(string: "http://www.google.com")!
            
            let part: [Any] = [url!.lastPathComponent]
         
            let filename: String? = part.last as? String

            let `extension`: String = (url?.pathExtension)!
            let check = `extension`
            print(check)
            if filename == "ShowPDF.aspx"
            {
                if #available(iOS 10.0, *)
                {
                    UIApplication.shared.open(url! , options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url! )
                }

            }
        }

    }
    
    func ViewLayout()
    {
        self.btn_table_title.layer.cornerRadius = 5
        self.btn_table_title.layer.borderWidth = 1
        self.btn_table_title.layer.borderColor = UIColor.gray.cgColor
        
        // view board style :
        self.view_casteYourVote.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_casteYourVote.layer.shadowOpacity = 0.5
        self.view_casteYourVote.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_casteYourVote.layer.shadowRadius = 10
        //self.view_casteYourVote.layer.cornerRadius = 20
        self.view_casteYourVote.layer.masksToBounds = false
        self.view_casteYourVote.layer.shadowPath = UIBezierPath(rect: self.view_casteYourVote.bounds).cgPath
        self.view_casteYourVote.layer.shouldRasterize = true
        self.view_casteYourVote.layer.rasterizationScale =  UIScreen.main.scale
        
        self.BILL_SUMMARY.layer.shadowColor = UIColor.lightGray.cgColor
        self.BILL_SUMMARY.layer.shadowOpacity = 0.5
        self.BILL_SUMMARY.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.BILL_SUMMARY.layer.shadowRadius = 10
        self.BILL_SUMMARY.layer.cornerRadius = 20
        self.BILL_SUMMARY.layer.masksToBounds = false
        self.BILL_SUMMARY.layer.shadowPath = UIBezierPath(rect: self.BILL_SUMMARY.bounds).cgPath
        self.BILL_SUMMARY.layer.shouldRasterize = true
        self.BILL_SUMMARY.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_userName.layer.borderWidth = 0.6
        self.view_userName.layer.borderColor = UIColor.lightGray.cgColor
        self.view_email.layer.borderWidth = 0.6
        self.view_email.layer.borderColor = UIColor.lightGray.cgColor
        
        self.view_userSignIn.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_userSignIn.layer.shadowOpacity = 0.5
        self.view_userSignIn.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_userSignIn.layer.shadowRadius = 10
        self.view_userSignIn.layer.cornerRadius = 20
        self.view_userSignIn.layer.masksToBounds = false
        self.view_userSignIn.layer.shadowPath = UIBezierPath(rect: self.view_userSignIn.bounds).cgPath
        self.view_userSignIn.layer.shouldRasterize = true
        self.view_userSignIn.layer.rasterizationScale =  UIScreen.main.scale
        
        self.VIEW_RATING.layer.shadowColor = UIColor.lightGray.cgColor
        self.VIEW_RATING.layer.shadowOpacity = 0.5
        self.VIEW_RATING.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.VIEW_RATING.layer.shadowRadius = 10
        self.VIEW_RATING.layer.cornerRadius = 20
        self.VIEW_RATING.layer.masksToBounds = false
        self.VIEW_RATING.layer.shadowPath = UIBezierPath(rect: self.VIEW_RATING.bounds).cgPath
        self.VIEW_RATING.layer.shouldRasterize = true
        self.VIEW_RATING.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_menu_11.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_menu_11.layer.shadowOpacity = 0.5
        self.view_menu_11.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_menu_11.layer.shadowRadius = 10
        self.view_menu_11.layer.cornerRadius = 20
        self.view_menu_11.layer.masksToBounds = false
        self.view_menu_11.layer.shadowPath = UIBezierPath(rect: self.view_menu_11.bounds).cgPath
        self.view_menu_11.layer.shouldRasterize = true
        self.view_menu_11.layer.rasterizationScale =  UIScreen.main.scale
        
    }
}
