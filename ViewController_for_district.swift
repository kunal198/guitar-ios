//
//  ViewController_for_district.swift
//  politraker
//
//  Created by brst on 28/06/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController_for_district: UIViewController , UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate{

    @IBOutlet weak var view_casteYourVote: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var BILL_SUMMARY: UIView!
    @IBOutlet weak var lbl_table_title: UILabel!
    @IBOutlet var view1: UIView!
    @IBOutlet weak var view_userSignIn: UIView!
    @IBOutlet weak var VIEW_RATING: UIView!
    @IBOutlet weak var view_userName: UIView!
    @IBOutlet weak var view_email: UIView!

    @IBOutlet var img_legislator: UIImageView!
    @IBOutlet var lbl_legislator_name: UILabel!
    @IBOutlet var lbl_party_name: UILabel!
    @IBOutlet var lbl_district_name: UILabel!
    @IBOutlet var lbl_votes: UILabel!
    @IBOutlet var star1: UIImageView!
    @IBOutlet var star2: UIImageView!
    @IBOutlet var star3: UIImageView!
    @IBOutlet var star4: UIImageView!
    @IBOutlet var star5: UIImageView!
    @IBOutlet var lbl_summary_title: UILabel!
    @IBOutlet var lbl_summary: UITextView!
    
    var data = NSMutableArray()
    var table_title = String()
    var checkCategory = String()
    var selected_legislator = String()
    var ratings = Int()
    var leg_id = String()
    
    var dict_bills = NSDictionary()
    var dict_category = NSDictionary()
    var array_bills = NSMutableArray()
    var array_billsSummary = NSMutableArray()
    var array_category = NSMutableArray()
    var array_categorySummary = NSMutableArray()
    var array_votes_yes = NSMutableArray()
    var array_votes_no = NSMutableArray()
    var urlStr = NSMutableArray()
    var index1 = Int()
    var count = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaults.standard.object(forKey: "item_selected_legislator") != nil
        {
            self.selected_legislator = UserDefaults.standard.object(forKey: "item_selected_legislator") as! String
        }
        
        self.viewOutllet()
        
        self.img_legislator.layer.cornerRadius = self.img_legislator.frame.size.width/2
        self.img_legislator.clipsToBounds = true
        
        data = ["H.R.1628: AMERICAN HEALTH CARE ACT OF 2017","S.1553: PAIN CAPABLE UNBORN CHILD PROTECTION ACT","H.R.2: MEDICARE ACCESS AND CHIP REAUTHRIZATION ACT","H.R.1628: AMERICAN HEALTH CARE ACT OF 2017","S.1553: PAIN CAPABLE UNBORN CHILD PROTECTION ACT","H.R.2: MEDICARE ACCESS AND CHIP REAUTHRIZATION ACT"]
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view1.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view1.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view1.addSubview(blurEffectView)
        }
       
     
    }
    override func viewWillAppear(_ animated: Bool)
    {
            lbl_table_title.text = table_title
            self.getting_legislatorDetails()
            self.tableView.reloadData()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:<<< Table view Function
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if checkCategory == "category"
        {
            return self.array_category.count
        }
        else
        {
            return self.array_bills.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell_EDUCATION_WORKFORCE
        if checkCategory == "category"
        {
          
            
             let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: self.array_category[indexPath.row] as! String , attributes: underlineAttribute)

            cell.view_category.isHidden = false
            cell.lbl_title.attributedText = underlineAttributedString
            cell.lbl_votes_yes.text = self.array_votes_yes[indexPath.row] as? String
            cell.lbl_votes_no.text = self.array_votes_no[indexPath.row] as? String
            let str = "SPONSOR: "
            let str2 = String(self.count)
            cell.lbl_sponsor_count.text = str+str2
            
        }
        else
        {
         
            cell.view_category.isHidden = true
            
             let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: self.array_bills[indexPath.row] as! String, attributes: underlineAttribute)

           cell.lbl_title.attributedText = underlineAttributedString
            
        }
       
    /*    let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: data[indexPath.row] as! String, attributes: underlineAttribute)
        
        cell.lbl_title.attributedText = underlineAttributedString*/
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.checkCategory == "category"
        {
        
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "education&workforce") as! ViewController_edication_workforce
            UserDefaults.standard.set(self.array_category[indexPath.row] as? String, forKey: "category_selected")
           // nextViewController.lbl_title_str.text = self.array_category[indexPath.row] as? String
            self.navigationController?.pushViewController(nextViewController, animated: true)

        }
        else
        {
            let data = self.array_billsSummary[indexPath.row] as? String
            print(data!)
            if data! == "0"
            {
                self.view1.isHidden = false
                self.BILL_SUMMARY.isHidden = false
                self.index1 = indexPath.row
                
                self.lbl_summary_title.text = self.array_bills[indexPath.row] as? String//underlineAttributedString
                self.lbl_summary.text = ""
            }
            else
            {
                self.view1.isHidden = false
                self.BILL_SUMMARY.isHidden = false
                self.index1 = indexPath.row
            //    let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
         //       let underlineAttributedString = NSAttributedString(string: self.array_bills[indexPath.row] as! String, attributes: underlineAttribute)
                self.lbl_summary_title.text = self.array_bills[indexPath.row] as? String//underlineAttributedString
                self.lbl_summary.text = self.array_billsSummary[indexPath.row] as? String
            }
        }
    }
    @IBAction func btn_bills(_ sender: Any)
    {
       // self.getting_Bills()
        lbl_table_title.text = "BILLS & VOTES"
        self.checkCategory = "bills"
        self.tableView.reloadData()
    }
    @IBAction func btn_category(_ sender: Any)
    {
      //   self.getting_Category()
        lbl_table_title.text = "CATEGORIES & VOTES"
        self.checkCategory = "category"
        self.tableView.reloadData()
    }
    @IBAction func btn_goback(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func casteYourVote(_ sender: Any)
    {
        if UserDefaults.standard.object(forKey: "loginCheck") != nil
        {
            let loginCheck = UserDefaults.standard.object(forKey: "loginCheck") as? Bool
            if loginCheck == true
            {
                self.VIEW_RATING.isHidden = false
                self.view_userSignIn.isHidden = true
                self.view1.isHidden = false
            }
            else
            {
                self.view_userSignIn.isHidden = false
                self.view1.isHidden = false
            }
        }
        else
        {
            self.view_userSignIn.isHidden = false
            self.view1.isHidden = false
        }
    }
    @IBAction func userSignIn_exit(_ sender: Any)
    {
        self.view_userSignIn.isHidden = true
        self.view1.isHidden = true
        
    }
    @IBAction func btn_userSignIn(_ sender: Any)
    {
        self.VIEW_RATING.isHidden = false
        self.view_userSignIn.isHidden = true
        self.view1.isHidden = false
    }
    @IBAction func btn_rating_exit(_ sender: Any)
    {
        self.VIEW_RATING.isHidden = true
        self.view1.isHidden = true
    }
    @IBAction func btn_billSummary_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.BILL_SUMMARY.isHidden = true
    }

    
    func getting_Bills()
    {
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=legislators_bill")!)
        request.httpMethod = "POST"
        
        let postString = "leg_id=\(leg_id)"
        request.httpBody = postString.data(using: .utf8)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary //Dictionary<String, AnyObject>
            if((json) != nil){
                let data = json?.value(forKey: "data") as!NSArray
               print(data)
                
                if  data.value(forKey: "values") is [NSArray]
                {
                    let resultarr = data.value(forKey: "values") as! NSArray
                    let resultarr2 = resultarr[0] as! NSArray
                    print(resultarr2)
                    for i in resultarr2
                    {
                        
                        let indexvalue = i as! NSDictionary
                        self.array_bills.add(indexvalue.value(forKey: "name_leb") as! String)
                        
                        
                        if  let val = indexvalue["summary_leb"] as? String {
                            print(val)
                            self.array_billsSummary.add(indexvalue.value(forKey: "summary_leb") as! String)
                            self.urlStr.add(indexvalue.value(forKey: "url") as! String)
                        } else
                        {
                            self.array_billsSummary.add("0")
                            self.urlStr.add(indexvalue.value(forKey: "url") as! String)

                            print("nill")
                        }
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }                        

                    }
                    
                    print(self.urlStr)
                }
                else
                {
                    //
                }
               
            }
        }
        task.resume()

    }
    
    func getting_Category()
    {
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=legislators_cat")!)
        request.httpMethod = "POST"
        
        let postString = "leg_id=\(leg_id)"
        request.httpBody = postString.data(using: .utf8)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            if((json) != nil){
                let data = json?.value(forKey: "data") as!NSArray
              // print(data)
                if  data.value(forKey: "values") is [NSArray]
                {
                    let resultarr = data.value(forKey: "values") as! NSArray
                    let resultarr2 = resultarr[0] as! NSArray
                    let count1 = data.value(forKey: "count") as! NSArray
                    print(count1)
                    self.count = count1[0] as! Int
                    
                    
                    for i in resultarr2
                    {
                        let indexvalue = i as! NSDictionary
                        print(indexvalue)
                        self.array_category.add(indexvalue.value(forKey: "name_cat") as! String)
                        self.array_votes_yes.add(indexvalue.value(forKey: "totalyea_leb") as! String)
                        self.array_votes_no.add(indexvalue.value(forKey: "totalnay_leb") as! String)
                        if  indexvalue.value(forKey: "summary_leb") is [String]
                        {
                            self.array_categorySummary.add(indexvalue.value(forKey: "summary_leb") as! String)
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }

                    }
                }
                else
                {
                    //
                }
            }
        }
        task.resume()

    }
    
    func getting_legislatorDetails()
    {
        // Getting  api data for Legislator
        legislatorData .gettingData(withSuccess: { (result) in
            let dictData = result as NSMutableArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.selected_legislator == dict.value(forKey: "name_leg") as! String
                {
                    let str111 = dict.value(forKey: "name_leg") as? String
                    self.lbl_legislator_name.text = str111?.uppercased()            //dict.value(forKey: "name_leg") as? String
                    let str112 = dict.value(forKey: "party_leg") as? String
                    self.lbl_party_name.text = str112?.uppercased()                     //dict.value(forKey: "party_leg") as? String
                    let str113 = dict.value(forKey: "name_dis") as? String
                    self.lbl_district_name.text = str113?.uppercased()          //dict.value(forKey: "name_dis") as? String
                    
                    self.leg_id = dict.value(forKey: "id_leg") as! String
                    self.ratings = (dict.value(forKey: "ratings") as? Int)!
                    
                    switch self.ratings
                    {
                    case 1:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "rating_star")
                        self.star3.image = UIImage(named: "rating_star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        
                        break
                    case 2:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "rating_star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        
                        break
                    case 3:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        break
                    case 4:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "star")
                        self.star4.image = UIImage(named: "star")
                        self.star5.image = UIImage(named: "rating_star")
                        break
                    case 5:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "star")
                        self.star4.image = UIImage(named: "star")
                        self.star5.image = UIImage(named: "star")
                        break
                    default:
                        self.star1.image = UIImage(named: "rating_star")
                        self.star2.image = UIImage(named: "rating_star")
                        self.star3.image = UIImage(named: "rating_star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        
                        break
                        
                    }
                    let votes = dict.value(forKey: "votes") as! Int
                    self.lbl_votes.text = ("\(votes)\("")\(" VOTES")")
                    
                    let img_url = dict.value(forKey: "img_url") as? String
                    
                    self.img_legislator.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "avtar"))
                    
                    self.getting_Bills()
                    self.getting_Category()
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }

                }
                
            }
            
            print(dictData)
        }, failure: { (error) in
            print (error)
        });

    }
    
    
    func viewOutllet()
    {
        
        // view board style :
        self.view_casteYourVote.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_casteYourVote.layer.shadowOpacity = 0.5
        self.view_casteYourVote.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_casteYourVote.layer.shadowRadius = 10
        //self.view_casteYourVote.layer.cornerRadius = 20
        self.view_casteYourVote.layer.masksToBounds = false
        self.view_casteYourVote.layer.shadowPath = UIBezierPath(rect: self.view_casteYourVote.bounds).cgPath
        self.view_casteYourVote.layer.shouldRasterize = true
        self.view_casteYourVote.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_userName.layer.borderWidth = 0.6
        self.view_userName.layer.borderColor = UIColor.lightGray.cgColor
        self.view_email.layer.borderWidth = 0.6
        self.view_email.layer.borderColor = UIColor.lightGray.cgColor
        
        self.view_userSignIn.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_userSignIn.layer.shadowOpacity = 0.5
        self.view_userSignIn.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_userSignIn.layer.shadowRadius = 10
        self.view_userSignIn.layer.cornerRadius = 20
        self.view_userSignIn.layer.masksToBounds = false
        self.view_userSignIn.layer.shadowPath = UIBezierPath(rect: self.view_userSignIn.bounds).cgPath
        self.view_userSignIn.layer.shouldRasterize = true
        self.view_userSignIn.layer.rasterizationScale =  UIScreen.main.scale
        
        self.VIEW_RATING.layer.shadowColor = UIColor.lightGray.cgColor
        self.VIEW_RATING.layer.shadowOpacity = 0.5
        self.VIEW_RATING.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.VIEW_RATING.layer.shadowRadius = 10
        self.VIEW_RATING.layer.cornerRadius = 20
        self.VIEW_RATING.layer.masksToBounds = false
        self.VIEW_RATING.layer.shadowPath = UIBezierPath(rect: self.VIEW_RATING.bounds).cgPath
        self.VIEW_RATING.layer.shouldRasterize = true
        self.VIEW_RATING.layer.rasterizationScale =  UIScreen.main.scale
        
        self.BILL_SUMMARY.layer.shadowColor = UIColor.lightGray.cgColor
        self.BILL_SUMMARY.layer.shadowOpacity = 0.5
        self.BILL_SUMMARY.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.BILL_SUMMARY.layer.shadowRadius = 10
        self.BILL_SUMMARY.layer.cornerRadius = 20
        self.BILL_SUMMARY.layer.masksToBounds = false
        self.BILL_SUMMARY.layer.shadowPath = UIBezierPath(rect: self.BILL_SUMMARY.bounds).cgPath
        self.BILL_SUMMARY.layer.shouldRasterize = true
        self.BILL_SUMMARY.layer.rasterizationScale =  UIScreen.main.scale

    }
    
    //MARK: Bill Summary Title _ PDF
    @IBAction func btn_billSummary_title_pdf(_ sender: Any)
    {
        if self.urlStr.count >= 0
        {
            let str = self.urlStr[index1] as! String
            let url = URL(string: str)  //URL(string: "http://www.google.com")!
            if #available(iOS 10.0, *)
            {
                UIApplication.shared.open(url! , options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url! )
            }
        }
    }
    
    
}
