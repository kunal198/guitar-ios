//
//  TableViewCell_GoBACK_2.swift
//  politraker
//
//  Created by brst on 27/06/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class TableViewCell_GoBACK_2: UITableViewCell {

    
    @IBOutlet weak var lbl_sponsor: UILabel!
    @IBOutlet weak var lbl_voted: UILabel!
    @IBOutlet weak var img_sponsor: UIImageView!
    @IBOutlet weak var img_voted: UIImageView!
    @IBOutlet weak var view_forCategoryOnly: UIView!
    @IBOutlet var view_forDistrict: UIView!
    
    @IBOutlet var lbl_legislator_name: UILabel!
    @IBOutlet var lbl_party_name: UILabel!
    @IBOutlet var img_legislator: UIImageView!
    @IBOutlet var lbl_district_name: UILabel!
    
    @IBOutlet var star1: UIImageView!
    @IBOutlet var star2: UIImageView!
    @IBOutlet var star3: UIImageView!
    @IBOutlet var star4: UIImageView!
    @IBOutlet var star5: UIImageView!
    @IBOutlet var lbl_Total_votes: UILabel!
    @IBOutlet var lbl_votes_no: UILabel!
    @IBOutlet var lbl_votes_yes: UILabel!
    
    @IBOutlet var lbl_sponsor_count: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
