//
//  ViewController2_SUBVIEW(GO_BACK).swift
//  politraker
//
//  Created by brst on 27/06/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController2_SUBVIEW_GO_BACK_: UIViewController , UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate{
    
    
    @IBOutlet weak var table_mainview: UIView!
    
    
    @IBOutlet weak var view_rating: UIView!
    @IBOutlet weak var view_userSignIn: UIView!
    @IBOutlet weak var view_userName: UIView!
    @IBOutlet weak var view_email: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view_casteYourVote: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbl_titleString: UILabel!
    @IBOutlet weak var lbl_titleAmericanHealthCare: UILabel!
    @IBOutlet weak var lbl_titleST_Thomas: UILabel!
    
    @IBOutlet var view_bills_selection: UIView!
    @IBOutlet var view11: UIView!
    @IBOutlet var lbl_billsANDCategory_title: UILabel!
    @IBOutlet var lbl_table_title: UILabel!
    @IBOutlet var tableView1: UITableView!
    @IBOutlet var lbl_data: UILabel!
    @IBOutlet var btn_title_str: UIButton!
    @IBOutlet var view_bill_summary: UIView!
    @IBOutlet var txtView_billsummary: UITextView!
    @IBOutlet var btn_billSummary_title: UIButton!
    @IBOutlet var btn_table_title: UIButton!
    @IBOutlet var lbl_billSummary_title: UILabel!
    
    var strBtnValue = String()
    var height = CGFloat()
    var strCell = String()
    var strCategory = Bool()
    var strDistrict = Bool()
    
    var selected_bills = String()
    var selected_district = String()
    var selected_category = String()
    
    var legislatorArray = NSMutableArray()
    var partyArray = NSMutableArray()
    var imgArray = NSMutableArray()
    var votesArray = NSMutableArray()
    var ratingsArray = NSMutableArray()
    var votes_yes = NSMutableArray()
    var votes_no = NSMutableArray()
    var billsArray = NSMutableArray()
    var categoryArray = NSMutableArray()
    var sponsorArray = NSMutableArray()
    
    var urlStr = String()
    var checkBillsBtn = Bool()
    
    var billSummaryArray = NSMutableArray()
    var strData = NSMutableArray()
    var str_leg = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        height = self.tableView.frame.size.height
        self.legislatorArray.removeAllObjects()
        self.partyArray.removeAllObjects()
        self.imgArray.removeAllObjects()
        
        if self.selected_bills == "" && self.selected_category == "" && self.selected_district == "" && UserDefaults.standard.object(forKey: "item_selected_legislator") != nil && UserDefaults.standard.object(forKey: "viewController2_subview") == nil
        {
            self.selected_bills = UserDefaults.standard.object(forKey: "item_selected_legislator") as! String
            legislatorData .gettingData(withSuccess: { (result) in
                let dictData = result as NSMutableArray
                for index in 0..<dictData.count
                {
                    var dict = NSDictionary()
                    dict = dictData[index] as! NSDictionary
                    self.legislatorArray.add(dict.value(forKey: "name_leg") as! String)
                    self.partyArray.add(dict.value(forKey: "party_leg") as! String)
                    self.imgArray.add(dict.value(forKey: "img_url") as! String)
                }
                
                self.tableView.reloadData()
            }, failure: { (error) in
                print (error)
            });
        }
        if UserDefaults.standard.object(forKey: "viewController2_subview") != nil && self.selected_bills == "" && self.selected_district == "" && self.selected_category == ""
        {
            self.strBtnValue = UserDefaults.standard.object(forKey: "viewController2_subview") as! String
            self.selected_district = UserDefaults.standard.object(forKey: "selected_btn") as! String
            self.selected_bills = UserDefaults.standard.object(forKey: "selected_btn") as! String
            self.selected_category = UserDefaults.standard.object(forKey: "selected_btn") as! String
        }
        if self.strBtnValue == "chooseBills"
        {
                self.getting_bills()
        }
        if self.strBtnValue == "chooseDistrict"
        {
            self.getting_district()
        }
        
        if self.strBtnValue == "chooseCategory"
        {
            self.getting_category()
        }

        
        self.viewOutlet()
        
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view1.addGestureRecognizer(tapGesture)

    }
       override func viewWillAppear(_ animated: Bool) {

        if strBtnValue == "chooseDistrict"
        {
            lbl_titleString.text = "ST. THOMAS LEGISLATORS"
            self.lbl_titleAmericanHealthCare.isHidden = true
            self.lbl_titleST_Thomas.isHidden = true
            self.lbl_titleString.isHidden = false
            self.table_mainview.frame.origin.y = self.lbl_titleString.frame.origin.y + self.lbl_titleString.frame.height
            let check = tableView.frame.size.height
            if  check == height
            {
                self.table_mainview.frame.size.height = self.table_mainview.frame.size.height  + self.lbl_titleString.frame.size.height
            }
            self.getting_district()
        }
        if strBtnValue == "chooseCategory"
        {
            lbl_titleString.text = "EDUCATION AND WORKFORCE"
            self.lbl_titleAmericanHealthCare.isHidden = true
            self.lbl_titleST_Thomas.isHidden = true
            self.lbl_titleString.isHidden = false
            self.table_mainview.frame.origin.y = self.lbl_titleString.frame.origin.y + self.lbl_titleString.frame.height
            let check = tableView.frame.size.height
            if  check == height
            {
                self.table_mainview.frame.size.height = self.table_mainview.frame.size.height  + self.lbl_titleString.frame.size.height
            }
            self.getting_category()
        }
        if strBtnValue == "chooseBills"
        {
            lbl_titleString.text = "EDUCATION AND WORKFORCE"
            self.lbl_titleAmericanHealthCare.isHidden = true
            self.lbl_titleST_Thomas.isHidden = true
            self.lbl_titleString.isHidden = false
            self.table_mainview.frame.origin.y = self.lbl_titleString.frame.origin.y + self.lbl_titleString.frame.height
            let check = tableView.frame.size.height
            if  check == height
            {
                self.table_mainview.frame.size.height = self.table_mainview.frame.size.height  + self.lbl_titleString.frame.size.height
            }
            self.getting_bills()
        }
        if strBtnValue != "chooseDistrict" && strBtnValue != "chooseCategory" && strBtnValue != "chooseBills"
        {
            self.lbl_titleString.isHidden = true
            self.lbl_titleST_Thomas.isHidden = false
            self.lbl_titleAmericanHealthCare.isHidden = false
        }
        self.getting_bills()
        self.getting_category()
     //   self.BillsAndCategory()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btn_GoBack(_ sender: Any)
    {
        
        self.navigationController?.popViewController(animated: true)
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "home") as! ViewController
//        self.navigationController?.pushViewController(nextViewController, animated: true)

//        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
//        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
    }
    
    
    //MARK: Table view Function for Legislator, District, Bills, Category
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView1
        {
            if self.checkBillsBtn == true
            {
                return self.billsArray.count
            }
            else
            {
                return self.categoryArray.count
            }
            
        }
        return self.legislatorArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableView1
        {
            let cell1 = tableView1.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCell_BillsOrCategory
            if self.checkBillsBtn == true
            {
                cell1.lbl.text = self.billsArray[indexPath.row] as? String
                return cell1
            }
            else
            {
                cell1.lbl.text = self.categoryArray[indexPath.row] as? String
                return cell1
            }

        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell_GoBACK_2
//        let img_url = imgArray[indexPath.row] as! String
        cell.img_legislator.layer.cornerRadius = cell.img_legislator.frame.width/2
        cell.img_legislator.clipsToBounds = true
        cell.img_legislator.frame.size.height = cell.img_legislator.frame.size.width
        cell.img_legislator.sd_setImage(with: URL(string: (imgArray[indexPath.row] as! NSString) as String), placeholderImage: UIImage(named: "avtar"))
        self.str_leg = (legislatorArray[indexPath.row] as? String)!
        cell.lbl_legislator_name.text = self.str_leg.uppercased()//legislatorArray[indexPath.row] as? String
        let str112 = partyArray[indexPath.row] as? String
        cell.lbl_party_name.text = str112?.uppercased()//partyArray[indexPath.row] as? String
    
        
        
        
        if strCategory == true || strDistrict == true
        {
            
            cell.lbl_Total_votes.text = ("\(self.votesArray[indexPath.row] )\(" votes")")
            let ratings = self.ratingsArray[indexPath.row] as! Int
            switch ratings
            {
            case 1:
                cell.star1.image = UIImage(named: "star")
                cell.star2.image = UIImage(named: "rating_star")
                cell.star3.image = UIImage(named: "rating_star")
                cell.star4.image = UIImage(named: "rating_star")
                cell.star5.image = UIImage(named: "rating_star")
                
                break
            case 2:
                cell.star1.image = UIImage(named: "star")
                cell.star2.image = UIImage(named: "star")
                cell.star3.image = UIImage(named: "rating_star")
                cell.star4.image = UIImage(named: "rating_star")
                cell.star5.image = UIImage(named: "rating_star")
                
                break
            case 3:
                cell.star1.image = UIImage(named: "star")
                cell.star2.image = UIImage(named: "star")
                cell.star3.image = UIImage(named: "star")
                cell.star4.image = UIImage(named: "rating_star")
                cell.star5.image = UIImage(named: "rating_star")
                break
            case 4:
                cell.star1.image = UIImage(named: "star")
                cell.star2.image = UIImage(named: "star")
                cell.star3.image = UIImage(named: "star")
                cell.star4.image = UIImage(named: "star")
                cell.star5.image = UIImage(named: "rating_star")
                break
            case 5:
                cell.star1.image = UIImage(named: "star")
                cell.star2.image = UIImage(named: "star")
                cell.star3.image = UIImage(named: "star")
                cell.star4.image = UIImage(named: "star")
                cell.star5.image = UIImage(named: "star")
                break
            default:
                cell.star1.image = UIImage(named: "rating_star")
                cell.star2.image = UIImage(named: "rating_star")
                cell.star3.image = UIImage(named: "rating_star")
                cell.star4.image = UIImage(named: "rating_star")
                cell.star5.image = UIImage(named: "rating_star")
                
                break
                
            }

            if strDistrict == true
            {
                cell.view_forCategoryOnly.isHidden = false
                cell.view_forDistrict.isHidden = false
                
                return cell
            }
            if strCategory == true
            {
                cell.view_forCategoryOnly.isHidden = false
                
                cell.lbl_votes_yes.text = ("\(self.votes_yes[indexPath.row] )")
                cell.lbl_votes_no.text = ("\(self.votes_no[indexPath.row] )")
                cell.lbl_sponsor_count.text = ("SPONSOR: \(self.sponsorArray[indexPath.row])")
                print(sponsorArray)
                print(cell.lbl_sponsor_count.text!)
            //  cell.lbl_votes_yes.text =(" self.votes_yes[indexPath.row]")
           //   cell.lbl_votes_no.text = self.votes_no[indexPath.row] as? Int
                
                return cell
            }
            return cell
        }

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tableView1
        {
            if self.checkBillsBtn == true
            {
                self.lbl_table_title.text = self.billsArray[indexPath.row] as?  String
                self.tableView1.isHidden = true
            }
            else
            {
                
                self.lbl_table_title.text = self.categoryArray[indexPath.row] as? String
                self.tableView1.isHidden = true
            }

        }
        else
        {
           // UserDefaults.standard.set(self.lbl_titleString.text, forKey: "titleString")
            if strBtnValue == "chooseDistrict"
            {
                lbl_titleString.text = "ST. THOMAS LEGISLATORS"
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                UserDefaults.standard.set(self.str_leg , forKey: "item_selected_legislator")

                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2") as! ViewController2
                nextViewController.selected_district = (self.legislatorArray[indexPath.row] as? String)!
                nextViewController.checkStr = "district"
                UserDefaults.standard.set((self.legislatorArray[indexPath.row] as? String)!, forKey: "item_selected_legislator")
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            if strBtnValue == "chooseCategory"
            {
 
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                UserDefaults.standard.set(self.str_leg, forKey: "item_selected_legislator")
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "education&workforce") as! ViewController_edication_workforce
              //  nextViewController.table_title = "BILLS & VOTES"
               // nextViewController.checkCategory = "bills"
               // UserDefaults.standard.set((self.legislatorArray[indexPath.row] as? String)!, forKey: "item_selected_legislator")
                self.navigationController?.pushViewController(nextViewController, animated: true)
            
            
            
            /*    lbl_titleString.text = "EDUCATION AND WORKFORCE"
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "education&workforce") as! ViewController_edication_workforce

            UserDefaults.standard.set((self.legislatorArray[indexPath.row] as? String)!, forKey: "item_selected_legislator")
            self.navigationController?.pushViewController(nextViewController, animated: true)
*/
//            self.view1.isHidden = false
//            self.view_casteYourVote.isHidden = false
            }
        if strBtnValue == "chooseBills"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2") as! ViewController2
            nextViewController.selected_legislator = (self.legislatorArray[indexPath.row] as? String)!
            nextViewController.checkStr = "bills"
            UserDefaults.standard.set(self.str_leg, forKey: "item_selected_legislator")
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        if strBtnValue != "chooseDistrict" && strBtnValue != "chooseCategory" && strBtnValue != "chooseBills"
        {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 2], animated: true);

//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2") as! ViewController2
//            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        }
    }
    //MARK: Gesture Tap Function:-
    func tapAction(sender:UITapGestureRecognizer)
    {
        if self.view_casteYourVote.isHidden == false
        {
            self.view_casteYourVote.isHidden = true
            self.view1.isHidden = true
        }
        self.view_bill_summary.isHidden = true
        self.view1.isHidden = true
    }
    @IBAction func btn_casteYourVote(_ sender: Any)
    {
        if UserDefaults.standard.object(forKey: "loginCheck") != nil
        {
            let loginCheck = UserDefaults.standard.object(forKey: "loginCheck") as? Bool
            if loginCheck == true
            {
                self.view_rating.isHidden = false
                self.view_userSignIn.isHidden = true
                //self.view1.isHidden = false
            }
            else
            {
                self.view_casteYourVote.isHidden = true
                self.view_userSignIn.isHidden = false
             //   self.view_userSignIn.isHidden = false
               // self.view1.isHidden = false
            }
        }
        else
        {
            self.view_casteYourVote.isHidden = true
            self.view_userSignIn.isHidden = false
            //self.view_userSignIn.isHidden = false
           // self.view1.isHidden = false
        }
//        self.view_casteYourVote.isHidden = true
//        self.view_userSignIn.isHidden = false
    }
    @IBAction func btn_userSignIn_exit(_ sender: Any)
    {
        self.view_userSignIn.isHidden = true
        self.view1.isHidden = true
    }
    @IBAction func btn_userSignIn_click(_ sender: Any)
    {
        self.view_userSignIn.isHidden = true
        self.view_rating.isHidden = false
    }
    @IBAction func btn_rating_exit(_ sender: Any)
    {
        self.view_rating.isHidden = true
        self.view1.isHidden = true
    }
    @IBAction func btn_bills_category_submit(_ sender: Any)
    {
        if self.lbl_billsANDCategory_title.text == "CHOOSE THE BILL"
        {
            //            UserDefaults.standard.set(self.lbl_tabletitle_str.text, forKey: "item_selected_bills")
            //
            //            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            //
            //            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            //            nextViewController.strBtnValue = "chooseBills"
            //            self.navigationController?.pushViewController(nextViewController, animated: true)
            
            UserDefaults.standard.set(self.lbl_table_title.text, forKey: "item_selected_bills")
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.strBtnValue = "chooseBills"
            nextViewController.selected_bills = self.lbl_table_title.text!    //self.selected_bills
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
        if self.lbl_billsANDCategory_title.text == "CHOOSE CATEGORY"
        {
            //            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            //
            //            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            //            nextViewController.strBtnValue = "chooseCategory"
            //            self.navigationController?.pushViewController(nextViewController, animated: true)
            
            UserDefaults.standard.set(self.lbl_table_title.text , forKey: "item_selected_category")
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.selected_category = self.lbl_table_title.text!   //self.selected_category
            nextViewController.strBtnValue = "chooseCategory"
            nextViewController.strCategory = true
            nextViewController.strDistrict = false
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }

        
//        if self.lbl_billsANDCategory_title.text == "CHOOSE THE BILL"
//        {
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
//            nextViewController.strBtnValue = "chooseBills"
//            self.navigationController?.pushViewController(nextViewController, animated: true)
//        }
//        if self.lbl_billsANDCategory_title.text == "CHOOSE CATEGORY"
//        {
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
//            nextViewController.strBtnValue = "chooseCategory"
//            nextViewController.strCategory = true
//            nextViewController.strDistrict = false
//            self.navigationController?.pushViewController(nextViewController, animated: true)
//        }

    }
    
    @IBAction func btn_table_title_clicked(_ sender: Any)
    {
        if self.tableView1.isHidden == false
        {
            self.tableView1.isHidden = true
        }
        else
        {
            self.tableView1.isHidden = true
          //  self.view1.isHidden = false
        }
    }
    @IBAction func btn_bills_category_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.view_bills_selection.isHidden = true
    }
    @IBAction func btn_bills_clicked(_ sender: Any)
    {
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "except_district") as! ViewController_for_district
//        nextViewController.table_title = "BILLS & VOTES"
//        nextViewController.checkCategory = "bills"
//        self.navigationController?.pushViewController(nextViewController, animated: true)

        
//        self.view1.isHidden = false
//        self.view_bills_selection.isHidden = false
//        self.lbl_billsANDCategory_title.text = "CHOOSE THE BILL"
//        self.lbl_table_title.text = self
//        self.strCell = "Vi12-Bill"
//        self.strCategory = false
//        self.tableView1.reloadData()
        
        
        self.checkBillsBtn = true
        self.view1.isHidden = false
        self.view_bills_selection.isHidden = false
        self.lbl_billsANDCategory_title.text = "CHOOSE THE BILL"
        self.lbl_table_title.text = self.billsArray[0] as? String
        self.tableView1.reloadData()

    }
    @IBAction func btn_category_clicked(_ sender: Any)
    {
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "except_district") as! ViewController_for_district
//        nextViewController.table_title = "CATEGORIES & VOTES"
//        nextViewController.checkCategory = "category"
//
//        self.navigationController?.pushViewController(nextViewController, animated: true)

        
        
//        self.view1.isHidden = false
//        self.view_bills_selection.isHidden = false
//        self.btn_table_title .setTitle("   Education", for: UIControlState.normal)
//        self.lbl_billsANDCategory_title.text = "CHOOSE CATEGORY"
//        self.strCell = "Education"
//        self.strCategory = true
//
//        self.tableView1.reloadData()
        
        self.checkBillsBtn = false
        
        self.view1.isHidden = false
        self.view_bills_selection.isHidden = false
        self.lbl_billsANDCategory_title.text = "CHOOSE CATEGORY"
        self.lbl_table_title.text = self.categoryArray[0] as? String
        self.tableView1.reloadData()

    }
    //MARK:<<< Title selected >>>>
    @IBAction func btn_title_string_selected(_ sender: Any)
    {
        self.txtView_billsummary.text = strData[0] as? String
        self.view1.isHidden = false
        self.view_bill_summary.isHidden = false
    }
    
    @IBAction func btn_billSummary_title_selected(_ sender: Any)
    {
        if self.urlStr.isEmpty == false
        {
            let url = URL(string: self.urlStr)  //URL(string: "http://www.google.com")!
            if #available(iOS 10.0, *)
            {
                UIApplication.shared.open(url! , options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url! )
            }
        }
    
    }
    func getting_category()
    {
        UserDefaults.standard.set("chooseCategory", forKey: "viewController2_subview")
        UserDefaults.standard.set(self.selected_category, forKey: "selected_btn")
        
//        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=categories")!)
//        request.httpMethod = "POST"
//        
//       // let postString = "leg_id=\(leg_id)"
//       // request.httpBody = postString.data(using: .utf8)
//        
//        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        request.addValue("Basic", forHTTPHeaderField: "Authorization")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        request.setValue("0", forHTTPHeaderField: "Content-Length")
//        
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            
//            guard let data = data, error == nil else {
//                return
//            }
//            
//            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary //Dictionary<String, AnyObject>
//              print(json!)
//            if((json?.value(forKey: "data")) != nil){
//                let data = json?.value(forKey: "data") as!NSArray
//                print(data)
//
//            }
//        
//        }
//        task.resume()
        
    
        // Getting  api data for Category
        categoryData .getting_Category_Data(withSuccess: { (result) in
           // print(result)
            let dictData = result as NSMutableArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
              //  print(dictData[index])
                if self.categoryArray.contains(dict.value(forKey: "cat_name") as! String)
                {   }
                else
                {
                    self.categoryArray.add(dict.value(forKey: "cat_name") as! String)
                }

                if self.selected_category == dict.value(forKey: "cat_name") as! String
                {
                   // print(dict)
                    if dict.value(forKey: "sponsor") != nil
                    {
                        self.lbl_data.isHidden = true
                        let dictData1 = dict.value(forKey: "sponsor") as! NSMutableArray
                     //   print(dictData1)
                        for index1 in 0..<dictData1.count
                        {
                            var dict1 = NSDictionary()
                            dict1 = dictData1[index1] as! NSDictionary
                         //   print(dict1)
                            let data11 = dict1.value(forKey: "name_leg") as! String
                            if self.legislatorArray.contains(data11)
                            {
                                
                            }
                            else
                            {
                            self.legislatorArray.add(dict1.value(forKey: "name_leg") as! String)
                            self.partyArray.add(dict1.value(forKey: "party_leg") as! String)
                            self.imgArray.add(dict1.value(forKey: "img_url") as! String)
                            self.ratingsArray.add(dict1.value(forKey: "ratings") as! Int)
                            self.votesArray.add(dict1.value(forKey: "votes") as! Int)
                            self.votes_yes.add(dict1.value(forKey: "total_yea") as! String)
                            self.votes_no.add(dict1.value(forKey: "total_nay") as! String)
                            self.sponsorArray.add(dict1.value(forKey: "count") as! Int)
                            }
                        }
                        let data = UserDefaults.standard.object(forKey: "item_selected_category") as? String
                        
                        self.lbl_titleString.text = data!.uppercased()
                        self.lbl_titleST_Thomas.text = data!.uppercased()
                    }
                    else
                    {
                        self.lbl_data.isHidden = false
                    }
                }
            }
            
            self.tableView.reloadData()
        }, failure: { (error) in
            print (error)
        });
        
    }
    func getting_district()
    {
        UserDefaults.standard.set("chooseDistrict", forKey: "viewController2_subview")
        UserDefaults.standard.set(self.selected_district, forKey: "selected_btn")
        // Getting  api data for District
        districtData .getting_District_Data(withSuccess: { (result) in
            let dictData = result as NSMutableArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
              //  print(dict)
                
                if self.selected_district == dict.value(forKey: "district_name") as! String
                {
                    let dictData1 = dict.value(forKey: "legislators") as! NSMutableArray
                 //   print(dictData1)
                    for index1 in 0..<dictData1.count
                    {
                        var dict1 = NSDictionary()
                        dict1 = dictData1[index1] as! NSDictionary
                        let data11 = dict1.value(forKey: "name_leg") as! String
                        if self.legislatorArray.contains(data11)
                        {
                            
                        }
                        else
                        {
                        self.legislatorArray.add(dict1.value(forKey: "name_leg") as! String)
                        self.partyArray.add(dict1.value(forKey: "party_leg") as! String)
                        self.imgArray.add(dict1.value(forKey: "img_url") as! String)
                        self.votesArray.add(dict1.value(forKey: "votes") as! Int)
                        self.ratingsArray.add(dict1.value(forKey: "ratings") as! Int)
                        }
                    }
                    let data = UserDefaults.standard.object(forKey: "item_selected_district") as? String
                    self.lbl_titleString.text = ("\(data!.uppercased())\(" ")\(self.legislatorArray.count)\(" ")\("LEGISLATOR")")
                 //   print(self.lbl_titleST_Thomas.text!)
                }
                //                    else
                //                    {
                //                        self.lbl_data.isHidden = false
                //
                //                    }
            }
            
            self.tableView.reloadData()
        }, failure: { (error) in
            print (error)
        });
    }
    
    func getting_bills()
    {
        self.btn_title_str.isHidden = false
        UserDefaults.standard.set("chooseBills", forKey: "viewController2_subview")
        UserDefaults.standard.set(self.selected_bills, forKey: "selected_btn")
        // Getting  api data for Bills
        billsData .getting_Bills_Data(withSuccess: { (result) in
            let dictData = result as NSArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                if self.billsArray.contains(dict.value(forKey: "bill_name") as! String)
                {   }
                else
                {
                    self.billsArray.add(dict.value(forKey: "bill_name") as! String)
                }
                if self.selected_bills == dict.value(forKey: "bill_name") as! String
                {
                    if dict.value(forKey: "values") != nil
                    {
                        self.lbl_data.isHidden = true
                       // print(dict)
                       // print(dict.value(forKey: "bill_url")!)
                        self.urlStr  = dict.value(forKey: "bill_url") as! String//.add(dict.value(forKey: "bill_url")!)
                        let dictData1 = dict.value(forKey: "values") as! NSArray
                     //   print(dictData1)
                        let data11 = (dictData1[0] as AnyObject).value(forKey: "name_leg")!
                        if self.legislatorArray.contains(data11)
                        {
                            
                        }
                        else
                        {
                        //  for _ in 0..<dictData1.count
                        //     {
                        print(dictData)
                        self.legislatorArray.addObjects(from: [(dictData1[0] as AnyObject).value(forKey: "name_leg")! ])
                        self.partyArray.addObjects(from: [(dictData1[0] as AnyObject).value(forKey: "party_leg")! ])
                        self.imgArray.add((dictData1[0] as AnyObject).value(forKey: "img_url")! )
                        self.billSummaryArray.add((dictData1[0] as AnyObject).value(forKey: "summary_leb")!)
                        self.strData.add((dictData1[0] as AnyObject).value(forKey: "summary_leb")!)

                        }
                        //    }
                        let data = UserDefaults.standard.object(forKey: "item_selected_bills") as? String
                        self.lbl_titleString.text = data!.uppercased()
                        self.lbl_billSummary_title.text = data!
                     // self.btn_billSummary_title.setTitle(data!, for: UIControlState.normal)
                   //  self.txtView_billsummary.text = (dictData[0] as AnyObject).value(forKey: "summary_leb")
                        self.lbl_titleST_Thomas.text = data!.uppercased()
                    }
                    else
                    {
                        self.lbl_data.isHidden = false
                    }
                    
                }
            }
            
            self.tableView.reloadData()
        }, failure: { (error) in
            print (error)
        });

    }
    
    //MARK: getting Bills & Categories
//    func BillsAndCategory()
//    {
//        billsData .getting_Bills_Data(withSuccess: { (result) in
//            print (result)
//            let dictData = result as NSArray
//            for index in 0..<dictData.count
//            {
//                var dict = NSDictionary()
//                dict = dictData[index] as! NSDictionary
//                print(dict)
//                if self.billsArray.contains(dict.value(forKey: "bill_name") as! String)
//                {   }
//                else
//                {
//                    self.billsArray.add(dict.value(forKey: "bill_name") as! String)
//                }
//            }
//            
//        }, failure: { (error) in
//            print (error)
//        });
//        
//        
//        // Getting  api data for Category
//        categoryData .getting_Category_Data(withSuccess: { (result) in
//            //   print (result)
//            let dictData = result as NSMutableArray
//            //  print(dictData)
//            for index in 0..<dictData.count
//            {
//                var dict = NSDictionary()
//                dict = dictData[index] as! NSDictionary
//                if self.categoryArray.contains(dict.value(forKey: "cat_name") as! String)
//                {   }
//                else
//                {
//                    self.categoryArray.add(dict.value(forKey: "cat_name") as! String)
//                }
//            }
//            
//        }, failure: { (error) in
//            print (error)
//        });
//        
//    }
//
//    
    
    func viewOutlet()
    {
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view1.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view1.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view1.addSubview(blurEffectView)
        }
        
        self.btn_table_title.layer.cornerRadius = 5
        self.btn_table_title.layer.borderWidth = 1
        self.btn_table_title.layer.borderColor = UIColor.gray.cgColor
        // view board style :
        self.view_casteYourVote.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_casteYourVote.layer.shadowOpacity = 0.5
        self.view_casteYourVote.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_casteYourVote.layer.shadowRadius = 10
        self.view_casteYourVote.layer.cornerRadius = 10
        self.view_casteYourVote.layer.masksToBounds = false
        self.view_casteYourVote.layer.shadowPath = UIBezierPath(rect: self.view_casteYourVote.bounds).cgPath
        self.view_casteYourVote.layer.shouldRasterize = true
        self.view_casteYourVote.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_userName.layer.borderWidth = 0.6
        self.view_userName.layer.borderColor = UIColor.lightGray.cgColor
        self.view_email.layer.borderWidth = 0.6
        self.view_email.layer.borderColor = UIColor.lightGray.cgColor
        
        self.view_userSignIn.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_userSignIn.layer.shadowOpacity = 0.5
        self.view_userSignIn.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_userSignIn.layer.shadowRadius = 10
        self.view_userSignIn.layer.cornerRadius = 20
        self.view_userSignIn.layer.masksToBounds = false
        self.view_userSignIn.layer.shadowPath = UIBezierPath(rect: self.view_userSignIn.bounds).cgPath
        self.view_userSignIn.layer.shouldRasterize = true
        self.view_userSignIn.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_rating.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_rating.layer.shadowOpacity = 0.5
        self.view_rating.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_rating.layer.shadowRadius = 10
        self.view_rating.layer.cornerRadius = 20
        self.view_rating.layer.masksToBounds = false
        self.view_rating.layer.shadowPath = UIBezierPath(rect: self.view_rating.bounds).cgPath
        self.view_rating.layer.shouldRasterize = true
        self.view_rating.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view11.layer.shadowColor = UIColor.lightGray.cgColor
        self.view11.layer.shadowOpacity = 0.5
        self.view11.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view11.layer.shadowRadius = 10
        self.view11.layer.cornerRadius = 20
        self.view11.layer.masksToBounds = false
        self.view11.layer.shadowPath = UIBezierPath(rect: self.view11.bounds).cgPath
        self.view11.layer.shouldRasterize = true
        self.view11.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_bill_summary.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_bill_summary.layer.shadowOpacity = 0.5
        self.view_bill_summary.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_bill_summary.layer.shadowRadius = 10
        self.view_bill_summary.layer.cornerRadius = 20
        self.view_bill_summary.layer.masksToBounds = false
        self.view_bill_summary.layer.shadowPath = UIBezierPath(rect: self.view_bill_summary.bounds).cgPath
        self.view_bill_summary.layer.shouldRasterize = true
        self.view_bill_summary.layer.rasterizationScale =  UIScreen.main.scale
        

    }
}
