//
//  ViewController.swift
//  politraker
//
//  Created by Brst on 6/23/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import AFNetworking

class ViewController: UIViewController, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, GIDSignInUIDelegate {
    @IBOutlet var view1: UIView!
    @IBOutlet var view_main_menu: UIView!
    @IBOutlet var view_mainMenu_aboutUs: UIView!
    @IBOutlet var view_mainMenu_ContactUS: UIView!
    @IBOutlet var txt_mainMenu_contacts_name: UITextField!
    @IBOutlet var txt_mainMenu_contact_email: UITextField!
    @IBOutlet var txtView_mainMenu_contact_msg: UITextView!
    @IBOutlet var view_mainMenu_UserSignIn: UIView!
    @IBOutlet var subView_mainmenu_userSignIn_userName: UIView!
    @IBOutlet var subView_mainMenu_userSignIn_email: UIView!
    @IBOutlet var view_mainMenu_newUser: UIView!
    @IBOutlet var view_mainMenu_NewUser_thanks: UIView!
    @IBOutlet var btn_election2018_list3: UIButton!
    
    @IBOutlet var view_legislator_btn: UIView!
    @IBOutlet var lbl_legislator_title: UILabel!
    @IBOutlet var btn_legislator_list: UIButton!
    @IBOutlet var btn_legislator_submit: UIButton!
    
    @IBOutlet var view_election2018_btn: UIView!
    @IBOutlet var lbl_election2018_title: UILabel!
    @IBOutlet var btn_election_list1: UIButton!
    @IBOutlet var btn_election2018_list2: UIButton!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var tableView_list1: UITableView!
    @IBOutlet var view2: UIView!
    @IBOutlet var lbl_legislator_list: UILabel!
    // Menu - New User
    
    @IBOutlet var subView_newUser_userName: UIView!
    @IBOutlet var txt_newUser_userName: UITextField!
    @IBOutlet var subView_newUser_firstName: UIView!
    @IBOutlet var txt_newUser_firstName: UITextField!
    @IBOutlet var subView_newUser_lastName: UIView!
    @IBOutlet var txt_newUser_lastName: UITextField!
    @IBOutlet var subView_newUser_email: UIView!
    @IBOutlet var txt_newUser_email: UITextField!
    @IBOutlet var subView_newUser_password: UIView!
    @IBOutlet var txt_newUser_password: UITextField!
    
    // Menu - User Sign in
    @IBOutlet var txt_userSign_email: UITextField!
    @IBOutlet var txt_userSign_password: UITextField!
    @IBOutlet var login: UIButton!
    
    // Menu - Forget Password 
    @IBOutlet var view_forgetPassword: UIView!
    @IBOutlet var subView_email: UIView!
    @IBOutlet var txt_forget_email: UITextField!
    
    
    var check_Election2018_btn = Bool()
    var check_election2018_list1 = Bool()
    var check_election2018_list2 = Bool()
    var check_election2018_list3 = Bool()
    var check1stClick = String()
    var checkBtn = Int()
    var btn_searchByIssue = Bool()
    var str_btn = String()
    var arrayLegislator = NSMutableArray()
    var arrayBill = NSMutableArray()
    var arrayCategory = NSMutableArray()
    var arraydistrict = NSMutableArray()
    var arrayElectionList1 = NSMutableArray()
    var arrayElectionList2 = NSMutableArray()
    var arrayElectionList3 = NSMutableArray()
    var strElection_btn = String()
    var IndexCheck = Int()
    var submitCheck = String()
    var checkImg = Bool()

    var legislatorArray = NSMutableArray()
    var billsArray = NSMutableArray()
    var districtArray = NSMutableArray()
    var categoryArray = NSMutableArray()
    
    var selected_legislator = String()
    var selected_bills = String()
    var selected_district = String()
    var selected_category = String()
    var loginCheck = Bool()
    var election2018_login_check = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.categoryArray.removeAllObjects()
        self.districtArray.removeAllObjects()
        self.legislatorArray.removeAllObjects()
        self.billsArray.removeAllObjects()
        
        self.viewOutlet()
                                                                                    //   self.legislatorData()
        self.election2018_login_check = false

        // Getting  api data for Legislator
        legislatorData .gettingData(withSuccess: { (result) in
         //   print (result)
            let dictData = result as NSMutableArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.legislatorArray.contains(dict.value(forKey: "name_leg") as! String)
                {
                
                }
                else
                {
                    self.legislatorArray.add(dict.value(forKey: "name_leg") as! String)
                }
            }
           
        }, failure: { (error) in
            print (error)
        });
        
        
        // Getting  api data for Bills
        
        
      /*
            var post  = String()
            var request = URLRequest(url: URL(string: "http://beta.brstdev.com/swipr/api/web/v1/users/register")!)
     //       var i = 1
            /*for (key1, value) in parameters {
                
                let new = "\(key1)=\(value)"
                post = post + new
                
                if(i < parameters.count){
                    post = post+"&"
                }
                
                i += 1
            }
            
            request.httpMethod = "POST"
            
            var jsonstring1 = NSMutableString()
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                jsonstring1 = NSMutableString(string: jsonString)
                
            } catch {
                print("error")
            }
            let jsonstring2 = jsonstring1.mutableCopy() as! String
            request.httpBody = jsonstring2.data(using: .utf8)*/
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                
                guard let data = data, error == nil else {
                    //Error(error! as NSError)
                    return
                }
                
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                
                if((json) != nil){
                    print(json)
                    //success(json!)
                }
                
            }
            task.resume()
            
        
        
        
        
        */
        
        
        billsData .getting_Bills_Data(withSuccess: { (result) in
            print (result)
            let dictData = result as NSArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                print(dict)
                if self.billsArray.contains(dict.value(forKey: "bill_name") as! String)
                {   }
                else
                {
                    self.billsArray.add(dict.value(forKey: "bill_name") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });

        
        
        // Getting  api data for District
        districtData .getting_District_Data(withSuccess: { (result) in
          //  print (result)
            let dictData = result as NSMutableArray
         //   print(dictData)
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.districtArray.contains(dict.value(forKey: "district_name") as! String)
                {   }
                else
                {
                    self.districtArray.add(dict.value(forKey: "district_name") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });
        
        // Getting  api data for Category
        categoryData .getting_Category_Data(withSuccess: { (result) in
         //   print (result)
            let dictData = result as NSMutableArray
          //  print(dictData)
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                if self.categoryArray.contains(dict.value(forKey: "cat_name") as! String)
                {   }
                else
                {
                    self.categoryArray.add(dict.value(forKey: "cat_name") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });

        
        arrayLegislator = ["JOHN DOE","JOHN DOE","JOHN DOE"]
        arrayBill = ["Vi12-Bill","Vi12-Bill","Vi12-Bill"]
        arrayCategory = ["EDUCATION","EDUCATION","EDUCATION"]
        arraydistrict = ["ST.THOMAS","ST.THOMAS","ST.THOMAS"]
        arrayElectionList1 = ["   Search by issue", "   issue 1","   issue 2"]
        arrayElectionList2 = ["   Search by candidate", "   Candidate 1", "   Candidate 2"]
        arrayElectionList3 = ["   Search by candidency", "   Senatorial", "   Gubernatorial"]
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view1.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view1.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view1.addSubview(blurEffectView)
            
            
            GIDSignIn.sharedInstance().uiDelegate = self
            
            // Uncomment to automatically sign in the user.
            // GIDSignIn.sharedInstance().signInSilently()
            
            
        }
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view1.addGestureRecognizer(tapGesture)
        
        }
    override func viewWillAppear(_ animated: Bool) {
        
        self.election2018_login_check = false
        
        self.btn_election_list1.isUserInteractionEnabled = true
        self.btn_election2018_list2.isUserInteractionEnabled = true
        self.btn_election2018_list3.isUserInteractionEnabled = true
        
        if UserDefaults.standard.object(forKey: "loginCheck") != nil
        {
            self.loginCheck = (UserDefaults.standard.object(forKey: "loginCheck") as? Bool)!
            if self.loginCheck == true
            {
                self.login.setTitle("USER SIGN OUT", for: UIControlState.normal)
            }
            else
            {
                self.login.setTitle("USER SIGN IN", for: UIControlState.normal)

            }
        }
        self.view1.isHidden = true
        self.view_main_menu.isHidden = true
        self.view_legislator_btn.isHidden = true
        self.view2.isHidden = true
        self.view_election2018_btn.isHidden = true
        
        // Election 2018 buttons title
        self.checkImg = false
        self.submitCheck = "data"
        self.check1stClick = "data"
      
        self.btn_election_list1.setTitle("  Search by issues", for: UIControlState.normal)
        self.btn_election2018_list2.setTitle("   Search by candidacy", for: UIControlState.normal)
        self.btn_election2018_list3.setTitle("   Search by candidates", for: UIControlState.normal)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: <<< Main Menu Buttons :  = $$$$$$$$$
    @IBAction func btn_main_menu(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view_main_menu.isHidden = false
        
          }
    @IBAction func btn_mainMenu_Home(_ sender: Any)
    {
        self.view_main_menu.isHidden = true
        self.view1.isHidden = true
    }
    @IBAction func btn_mainMenu_AboutUs(_ sender: Any)
    {
        self.view_mainMenu_aboutUs.isHidden = false
        self.view1.isUserInteractionEnabled = false
       
    }
    @IBAction func btn_mainMenu_AboutUS_Exit(_ sender: Any)
    {
        self.view_mainMenu_aboutUs.isHidden = true
        self.view1.isUserInteractionEnabled = true
    }
    @IBAction func btn_mainMenu_ContactUs(_ sender: Any)
    {
        self.view_mainMenu_ContactUS.isHidden = false
        self.view1.isUserInteractionEnabled = false
        
    }
    @IBAction func btn_mainMenu_ContactUS_exit(_ sender: Any)
    {
        self.view_mainMenu_ContactUS.isHidden = true
        self.view1.isUserInteractionEnabled = true
    }
    @IBAction func btn_mainMenu_UserSignIn(_ sender: Any)
    {
        if UserDefaults.standard.object(forKey: "loginCheck") != nil
        {
            loginCheck = (UserDefaults.standard.object(forKey: "loginCheck") as? Bool)!
            if loginCheck == true
            {
                let alert = UIAlertController(
                    title: "ALERT",
                    message: "Are You Sure To Sign Out",
                    preferredStyle: UIAlertControllerStyle.alert)
            
                let loginAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                    self.loginCheck = false
                    UserDefaults.standard.set(self.loginCheck, forKey: "loginCheck")
                    self.login.setTitle("USER SIGN IN", for: UIControlState.normal)
                    self.view1.isHidden = true
                    self.view_main_menu.isHidden = true
                }
                let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default)
                {
                    UIAlertAction in
                }
                alert.addAction(loginAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.view1.isUserInteractionEnabled = false
                self.view_mainMenu_UserSignIn.isHidden = false
            }
        }
        
    }
    @IBAction func btn_mainMenu_UserSignIn_Exit(_ sender: Any)
    {
        self.view_mainMenu_UserSignIn.isHidden = true
        self.view1.isUserInteractionEnabled = true
        self.txt_userSign_email.text = ""
        self.txt_userSign_password.text = ""
    }
   
    @IBAction func btn_mainMenu_NewUserSignIn(_ sender: Any)
    {
        self.view1.isUserInteractionEnabled = false
        self.view_mainMenu_newUser.isHidden = false
    }
    //MARK:<<< Main Menu <Login User>
    @IBAction func btn_mainMenu_SignIn(_ sender: Any)
    {
        if self.txt_userSign_email.text != "" && self.txt_userSign_password.text != ""
        {
            self.loginData()
        }
        else
        {
            if self.txt_userSign_email.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER EMAIL ID",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)

            }
            if self.txt_userSign_password.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER PASSWORD",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)

            }
        }
    }
    
    //MARK: <<< Main Menu <New User Sign Up>
    @IBAction func btn_mainMenu_newUser_signUP(_ sender: Any)
    {
        if self.txt_newUser_userName.text != "" && self.txt_newUser_firstName.text != "" && self.txt_newUser_lastName.text != "" && self.txt_newUser_email.text != "" && self.txt_newUser_password.text != ""
        {
            self.PostData()
            self.txt_newUser_userName.text = ""
            self.txt_newUser_firstName.text = ""
            self.txt_newUser_lastName.text = ""
            self.txt_newUser_email.text = ""
            self.txt_newUser_password.text = ""
        }
        else
        {
            if self.txt_newUser_userName.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER USER NAME",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
            }
            if self.txt_newUser_firstName.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER FIRST NAME",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)

            }
            if self.txt_newUser_lastName.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER LAST NAME",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)

            }
            if self.txt_newUser_email.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER EMAIL ID",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)

            }
            if self.txt_newUser_password.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER PASSWORD",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)

            }
        }
       
    }
    @IBAction func btn_newUser_forgetPassword_btn(_ sender: Any)
    {
        self.view_mainMenu_UserSignIn.isHidden = true
        self.view_forgetPassword.isHidden = false
    }
    
    //MARK:<<< Main Menu <Forget Password> $$$$$$$$$$$$
    @IBAction func btn_forget_submit(_ sender: Any)
    {
        self.txt_forget_email.text = ""
        self.forgetData()
    }
    @IBAction func btn_forget_exit(_ sender: Any)
    {
        self.view_forgetPassword.isHidden = true
        self.txt_forget_email.text = ""
    }
    
    @IBAction func btn_mainMenu_exit(_ sender: Any)
    {
        self.view_main_menu.isHidden = true
        self.view1.isHidden = true
    }
    @IBAction func btn_electionView_exit(_ sender: Any)
    {
        self.view_election2018_btn.isHidden = true
        self.view1.isHidden = true
        
    }
    @IBAction func btn_legislatorView_exit(_ sender: Any)
    {
        self.view2.isHidden = true
        self.view1.isHidden = true
    }
    @IBAction func btn_mainMenu_newUser_exit(_ sender: Any)
    {
        self.view_mainMenu_newUser.isHidden = true
        self.view_mainMenu_NewUser_thanks.isHidden = true
        self.view1.isUserInteractionEnabled = true
        self.txt_newUser_userName.text = ""
        self.txt_newUser_firstName.text = ""
        self.txt_newUser_lastName.text = ""
        self.txt_newUser_email.text = ""
        self.txt_newUser_password.text = ""
    }
   
    
    //MARK:<<<  Legislator Submit Button
    @IBAction func btn_legislator_Submit(_ sender: Any)
    {
        if str_btn == "chooseLegislator"
        {
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_legislator")

            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2") as! ViewController2
            nextViewController.selected_legislator = self.lbl_legislator_list.text! //self.selected_legislator
            nextViewController.checkStr = "legislator"
            UserDefaults.standard.removeObject(forKey: "viewController2_subview")
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //
        if str_btn == "chooseBills"
        {
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_bills")

            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.strBtnValue = "chooseBills"
            nextViewController.selected_bills = self.lbl_legislator_list.text!    //self.selected_bills
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        if str_btn == "chooseDistrict"
        {
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_district")

            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.selected_district = self.lbl_legislator_list.text!   //self.selected_district
            nextViewController.strBtnValue = "chooseDistrict"
            nextViewController.strCategory = true
            nextViewController.strDistrict = true
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        if str_btn == "chooseCategory"
        {
            UserDefaults.standard.set(self.lbl_legislator_list.text , forKey: "item_selected_category")

            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.selected_category = self.lbl_legislator_list.text!   //self.selected_category
            nextViewController.strBtnValue = "chooseCategory"
            nextViewController.strCategory = true
            nextViewController.strDistrict = false
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
    @IBAction func btn_legislator_selected(_ sender: Any)
    {
        self.view1.isHidden = false
       self.view2.isHidden = false
        self.view_legislator_btn.isHidden = false
        self.lbl_legislator_title.text = "CHOOSE LEGISLATOR"
        self.lbl_legislator_list.text = self.legislatorArray[0] as? String                    //"JOHN DOE"
        str_btn = "chooseLegislator"
        check_Election2018_btn = false
        check_election2018_list1 = false
    }
    
    //MARK: <<< District Button
    @IBAction func btn_district_selected(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_legislator_btn.isHidden = false
        self.lbl_legislator_title.text = "CHOOSE DISTRICT"
        self.lbl_legislator_list.text = self.districtArray[0] as? String      //"ST.THOMAS"
        check_Election2018_btn = false
        check_election2018_list1 = false
        str_btn = "chooseDistrict"

    }
    
    //MARK: <<< Bills Button
    @IBAction func btn_bills_selected(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_legislator_btn.isHidden = false
        self.lbl_legislator_title.text = "CHOOSE THE BILLS"
        self.lbl_legislator_list.text = self.billsArray[0] as? String         //"Vi12-Bill"
        check_Election2018_btn = false
        check_election2018_list1 = false
        str_btn = "chooseBills"
    }
    //MARK:<<<  Categories Button
    @IBAction func btn_categories_selected(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_legislator_btn.isHidden = false
        self.lbl_legislator_title.text = "CHOOSE CATEGORY"
        self.lbl_legislator_list.text = self.categoryArray[0] as? String       //"EDUCATION"
        check_Election2018_btn = false
        check_election2018_list1 = false
        str_btn = "chooseCategory"

    }
    @IBAction func btn_list_selected(_ sender: Any)
    {
        if self.tableView.isHidden == false
        {
            self.tableView.isHidden = true
        }
        else
        {
            self.tableView.isHidden = false
            self.tableView.reloadData()
        }
    }
    //MARK: <<< btn Election2018
    @IBAction func btn_election2018_selected(_ sender: Any)
    {
        self.btn_election_list1.isUserInteractionEnabled = true
        self.btn_election2018_list2.isUserInteractionEnabled = true
        self.btn_election2018_list3.isUserInteractionEnabled = true
        self.view1.isHidden = false
        self.view_election2018_btn.isHidden = false
    }
    @IBAction func btn_election2018_list1(_ sender: Any)
    {
        if self.tableView_list1.isHidden == false
        {
            self.tableView_list1.isHidden = true
        }
        else
        {
            self.tableView_list1.isHidden = false
        }
        self.strElection_btn = ""
        check_Election2018_btn = true
        check_election2018_list1 = true
        check_election2018_list2 = false
        check_election2018_list3 = false
        self.tableView_list1.frame.origin.y = self.btn_election_list1.frame.origin.y + self.btn_election_list1.frame.size.height
        self.tableView_list1.reloadData()
    }
    @IBAction func btn_election_list2(_ sender: Any)
    {
        if self.tableView_list1.isHidden == false
        {
            self.tableView_list1.isHidden = true
        }
        else
        {
            self.tableView_list1.isHidden = false
        }
       // self.tableView_list1.isHidden = false
        check_Election2018_btn = true
        check_election2018_list1 = false
        check_election2018_list2 = true
        check_election2018_list3 = false

        self.tableView_list1.frame.origin.y = self.btn_election2018_list2.frame.origin.y + self.btn_election2018_list2.frame.size.height
        self.tableView_list1.reloadData()
    }
    @IBAction func btn_election_list3(_ sender: Any)
    {
        if self.tableView_list1.isHidden == false
        {
            self.tableView_list1.isHidden = true
        }
        else
        {
            self.tableView_list1.isHidden = false
        }
        //self.tableView_list1.isHidden = false
        check_Election2018_btn = true
        check_election2018_list1 = false
        check_election2018_list2 = false
        check_election2018_list3 = true
        self.tableView_list1.frame.origin.y = self.btn_election2018_list3.frame.origin.y + self.btn_election2018_list3.frame.size.height
        self.tableView_list1.reloadData()

    }
    
    
   //MARK: <<< Gesture function : Tap Action
    func tapAction(sender:UITapGestureRecognizer)
    {
        self.checkImg = false
        self.submitCheck = "data"
        self.check1stClick = "data"
        self.tableView_list1.reloadData()
        if tableView.isHidden == true && tableView_list1.isHidden == true
        {
            self.view_main_menu.isHidden = true
            self.view1.isHidden = true
            self.view2.isHidden = true
            self.view_legislator_btn.isHidden = true
            self.view_election2018_btn.isHidden = true
            
            self.btn_election_list1.setTitle("  Search by issues", for: UIControlState.normal)
            self.btn_election2018_list2.setTitle("   Search by candidacy", for: UIControlState.normal)
            self.btn_election2018_list3.setTitle("   Search by candidates", for: UIControlState.normal)
        }
        if tableView.isHidden == false
        {
            self.tableView.isHidden = true
        }
        if tableView_list1.isHidden == false
        {
            self.tableView_list1.isHidden = true
        }

    }
    //MARK: <<< Table view Function for Legislator, District, Bills, Category
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if check_Election2018_btn == true
        {
            return self.arrayElectionList1.count
        }
        else
        {
            if self.lbl_legislator_title.text == "CHOOSE LEGISLATOR"
            {
                return self.legislatorArray.count
            }
            if self.lbl_legislator_title.text == "CHOOSE THE BILLS"
            {
                return self.billsArray.count
            }
            if self.lbl_legislator_title.text == "CHOOSE CATEGORY"
            {
                return self.categoryArray.count
            }
            if self.lbl_legislator_title.text == "CHOOSE DISTRICT"
            {
                return self.districtArray.count
            }
            return self.arrayLegislator.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        if check_Election2018_btn == true
        {
                if check_election2018_list1 == true
                {
                    cell.lbl.text = arrayElectionList1[indexPath.row] as? String
                    return cell
                }
                if check_election2018_list2 == true
                {
                    cell.lbl.text = arrayElectionList2[indexPath.row] as? String
                    return cell
                }
                if  check_election2018_list3  == true
                {
                    cell.lbl.text = arrayElectionList3[indexPath.row] as? String
                    return cell
                }
        }
        else
        {
            if self.lbl_legislator_title.text == "CHOOSE LEGISLATOR"
            {
                cell.lbl.text = self.legislatorArray[indexPath.row] as? String
                return cell

            }
            if self.lbl_legislator_title.text == "CHOOSE THE BILLS"
            {
                cell.lbl.text = billsArray[indexPath.row] as? String
                return cell
            }
            if self.lbl_legislator_title.text == "CHOOSE CATEGORY"
            {
                cell.lbl.text = categoryArray[indexPath.row] as? String
                return cell

            }
            if self.lbl_legislator_title.text == "CHOOSE DISTRICT"
            {
                cell.lbl.text = self.districtArray[indexPath.row] as? String
                return cell

            }

        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if check_Election2018_btn == true
        {
            self.IndexCheck = indexPath.row
            self.tableView_list1.isHidden = true
            if check_election2018_list1 == true
            {
                self.btn_election_list1 .setTitle(self.arrayElectionList1[indexPath.row] as? String, for: UIControlState.normal)
                self.btn_election2018_list2.setTitle("   Search by candidacy", for: UIControlState.normal)
                self.btn_election2018_list3.setTitle("   Search by candidates", for: UIControlState.normal)
                if self.IndexCheck != 0
                {
                    self.btn_election2018_list2.isUserInteractionEnabled = false
                    self.btn_election2018_list3.isUserInteractionEnabled = false
                }
                else
                {
                    self.btn_election2018_list2.isUserInteractionEnabled = true
                    self.btn_election2018_list3.isUserInteractionEnabled = true

                }

            }
            if check_election2018_list2 == true
            {
                self.btn_election2018_list2.setTitle(self.arrayElectionList2[indexPath.row] as? String, for: UIControlState.normal)
                self.btn_election_list1.setTitle("   Search by issues", for: UIControlState.normal)
                self.btn_election2018_list3.setTitle("  Search by candidates", for: UIControlState.normal)
                if self.IndexCheck != 0
                {
                    self.btn_election_list1.isUserInteractionEnabled = false
                    self.btn_election2018_list3.isUserInteractionEnabled = false
                }
                else
                {
                    self.btn_election_list1.isUserInteractionEnabled = true
                    self.btn_election2018_list3.isUserInteractionEnabled = true
                    
                }

            }
            if check_election2018_list3 == true
            {
                self.btn_election2018_list3.setTitle(self.arrayElectionList3[indexPath.row] as? String, for: UIControlState.normal)
                self.btn_election2018_list2.setTitle("  Search by candidacy", for: UIControlState.normal)
                self.btn_election_list1.setTitle("  Search by issues", for: UIControlState.normal)
                if self.IndexCheck != 0
                {
                    self.btn_election2018_list2.isUserInteractionEnabled = false
                    self.btn_election_list1.isUserInteractionEnabled = false
                }
                else
                {
                    self.btn_election2018_list2.isUserInteractionEnabled = true
                    self.btn_election_list1.isUserInteractionEnabled = true
                    
                }

            }

            
        }
        else
        {
            if self.lbl_legislator_title.text == "CHOOSE LEGISLATOR"
            {
                self.lbl_legislator_list.text = self.legislatorArray[indexPath.row] as? String
                self.selected_legislator = (self.legislatorArray[indexPath.row] as? String)!
                self.tableView.isHidden = true
            }
            if self.lbl_legislator_title.text == "CHOOSE THE BILLS"
            {
                self.lbl_legislator_list.text = self.billsArray[indexPath.row] as? String
                self.selected_bills = (self.billsArray[indexPath.row] as? String)!
                self.tableView.isHidden = true
            }
            if self.lbl_legislator_title.text == "CHOOSE DISTRICT"
            {
                self.lbl_legislator_list.text = self.districtArray[indexPath.row] as? String
                self.selected_district = (self.districtArray[indexPath.row] as? String)!
                self.tableView.isHidden = true

            }
            if self.lbl_legislator_title.text == "CHOOSE CATEGORY"
            {
                self.lbl_legislator_list.text = self.categoryArray[indexPath.row] as? String
                self.selected_category = (self.categoryArray[indexPath.row] as? String)!
                self.tableView.isHidden = true
            }

        }
    }
    @IBAction func btn_election2018_submit_clicked(_ sender: Any)
    {
        if IndexCheck != 0
        {
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "election2018") as! ViewController_election2018_2
                        if check_election2018_list1 == true
                        {
                            nextViewController.checkStr = "issues"
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        if check_election2018_list2 == true
                        {
                            if UserDefaults.standard.object(forKey: "loginCheck") as? Bool != nil
                            {
                                self.loginCheck = (UserDefaults.standard.object(forKey: "loginCheck") as? Bool)!
                            }
                            else
                            {
                                self.loginCheck = false
                            }
                            if loginCheck == true
                            {
                                nextViewController.checkStr = "candidates"
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                            }
                            else
                            {
                                self.view1.isHidden = false
                                self.view_mainMenu_UserSignIn.isHidden = false
                                self.election2018_login_check = true

                            }
                            
                        }
                        if check_election2018_list3 == true
                        {
                            nextViewController.checkStr = "candidacy"
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
            
        }
        
    }
    func curren_selected()
    {
        if check_election2018_list1 == true
        {
            self.btn_election2018_list2.backgroundColor = UIColor.lightGray
            self.btn_election2018_list2.layer.borderColor = UIColor.lightGray.cgColor
            self.btn_election2018_list2.isUserInteractionEnabled = true
            
            self.btn_election2018_list3.backgroundColor = UIColor.lightGray
            self.btn_election2018_list3.layer.borderColor = UIColor.lightGray.cgColor
            self.btn_election2018_list3.isUserInteractionEnabled = true
        }
        if check_election2018_list2 == true
        {
            self.btn_election_list1.backgroundColor = UIColor.lightGray
            self.btn_election_list1.layer.borderColor = UIColor.lightGray.cgColor
            self.btn_election_list1.isUserInteractionEnabled = true
            
            self.btn_election2018_list3.backgroundColor = UIColor.lightGray
            self.btn_election2018_list3.layer.borderColor = UIColor.lightGray.cgColor
            self.btn_election2018_list3.isUserInteractionEnabled = true
            
        }
        if check_election2018_list3 == true
        {
            self.btn_election_list1.backgroundColor = UIColor.lightGray
            self.btn_election_list1.layer.borderColor = UIColor.lightGray.cgColor
            self.btn_election_list1.isUserInteractionEnabled = true
            
            self.btn_election2018_list2.backgroundColor = UIColor.lightGray
            self.btn_election2018_list2.layer.borderColor = UIColor.lightGray.cgColor
            self.btn_election2018_list2.isUserInteractionEnabled = true
        }
    }
    
    func undo()
    {
        self.btn_election_list1.backgroundColor = UIColor.white
        self.btn_election_list1.layer.borderColor = UIColor.lightGray.cgColor
        self.btn_election_list1.isUserInteractionEnabled = true
        
        self.btn_election2018_list2.backgroundColor = UIColor.white
        self.btn_election2018_list2.layer.borderColor = UIColor.lightGray.cgColor
        self.btn_election2018_list2.isUserInteractionEnabled = true
        
        self.btn_election2018_list3.backgroundColor = UIColor.white
        self.btn_election2018_list3.layer.borderColor = UIColor.lightGray.cgColor
        self.btn_election2018_list3.isUserInteractionEnabled = true
    }
   //MARK:<<< Menu - New User Sign Up : Facebook
    @IBAction func btn_facebook_signUp(_ sender: Any)
    {
        
    }
    
    //MARK: <<< Menu - New User Sign Up: Google
    @IBAction func btn_google_signUp(_ sender: Any) {
    }
    
    //MARK:<<<  Menu - New User Sign Up: Twitter
    @IBAction func btn_twitter_signUp(_ sender: Any) {
    }
    
//MARK: Sign Up Post Data -
    func PostData(){
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        let parameters: [String: String] = ["username": self.txt_newUser_userName.text!, "first_name" : self.txt_newUser_firstName.text!, "lastname": self.txt_newUser_lastName.text!, "email": txt_newUser_email.text!, "password": self.txt_newUser_password.text!]
        
        
        
        manager.post("http://www.politrackervi.com/apis/signup.php", parameters: parameters, progress: nil, success: {(_ operation: URLSessionDataTask, _ responseObject: Any) -> Void in
            
            
            do {
                let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                
                let description: String = jsonDict?.value(forKey: "description") as! String
                let status : String = jsonDict?.value(forKey: "status") as! String
                if status == "400"
                {
                    let alert = UIAlertController(
                        title: "ALERT",
                        message: description,
                        preferredStyle: UIAlertControllerStyle.alert)
                    
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                }
                else if status == "200"
                {
                    self.view_mainMenu_newUser.isHidden = true
                    self.view_mainMenu_NewUser_thanks.isHidden = false
                    
                    self.txt_newUser_userName.text = ""
                    self.txt_newUser_firstName.text = ""
                    self.txt_newUser_lastName.text = ""
                    self.txt_newUser_email.text = ""
                    self.txt_newUser_password.text = ""
                    
                    self.view_mainMenu_newUser.isHidden = true
                    self.view_mainMenu_NewUser_thanks.isHidden = false
                    // Thanks screen
                    
                }
                else if status == "201"
                {
                    let alert = UIAlertController(
                        title: "ALERT",
                        message: description,
                        preferredStyle: UIAlertControllerStyle.alert)
                    
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            catch{
                print("throws exception")
            }
            
        }, failure: {( task : URLSessionDataTask!, error: Error!) -> Void in
            
            let alert = UIAlertController(
                title: "Error",
                message: "CAN'T SIGN UP, WITH THESE USER DETAILS",
                preferredStyle: UIAlertControllerStyle.alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            }
            
            alert.addAction(OKAction)
            
            self.present(alert, animated: true, completion: nil)

            print("Error: \(error)")
        })
    }
    
//MARK: Sign In login Data -
    func loginData() {
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        let parameters: [String: String] = ["username": self.txt_userSign_email.text!, "password" : self.txt_userSign_password.text!]
            
        manager.post(
            "http://www.politrackervi.com/apis/login.php",
            parameters: parameters, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
               
                do {
                     let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    let description: String = jsonDict?.value(forKey: "description") as! String
                    let status : String = jsonDict?.value(forKey: "status") as! String
                    if status == "400"
                    {
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if status == "200"
                    {
                        self.view1.isUserInteractionEnabled = true
                        self.view_mainMenu_UserSignIn.isHidden = true
                        self.view_main_menu.isHidden = true
                        self.view1.isHidden = true
                        self.loginCheck = true
                        self.login.setTitle("USER SIGN OUT", for: UIControlState.normal)
                        UserDefaults.standard.set(self.loginCheck, forKey: "loginCheck")
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                            if self.election2018_login_check == true
                            {
                                self.election2018_login_check = false
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                
                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "election2018") as! ViewController_election2018_2
                                nextViewController.checkStr = "candidates"
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                            }
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                        self.txt_userSign_email.text = ""
                        self.txt_userSign_password.text = ""
                        
                       
                    }

                }catch{
                    
                }
                
            },
            failure:
            {
                (operation, error) in
                
                print(error)
                let alert = UIAlertController(
                    title: "Error",
                    message: "USER DOESN'T EXIST",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                print("Error: " + error.localizedDescription)
        })
    }

//MARK: Forget Password -
    func forgetData()
    {
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        let parameters: [String: String] = ["email": self.txt_forget_email.text!]
        
        manager.post(
            "http://www.politrackervi.com/apis/forgot_pasword.php",
            parameters: parameters, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    let description: String = jsonDict?.value(forKey: "description") as! String
                    let status : String = jsonDict?.value(forKey: "status") as! String
                    if status == "400"
                    {
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if status == "200"
                    {
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }catch{
                    
                }
                
            },
            failure:
            {
                (operation, error) in
                
                print(error)
                let alert = UIAlertController(
                    title: "Error",
                    message: "USER DOESN'T EXIST",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                print("Error: " + error.localizedDescription)
        })


    }
    
//MARK: <<< Legislator data &&&&&&&&&&&&&&&
  /*  func legislatorData ()
    {
        self.legislatorArray.removeAllObjects()
        self.districtArray.removeAllObjects()
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        
        manager.get(
            "http://www.politrackervi.com/apis/legistalors.php",
            parameters: nil, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                   
                    let dataDict = jsonDict?.object(forKey: "data") as! NSMutableArray
                    print("dataDict = \(String(describing: dataDict))")

                    
                    for index in 0..<dataDict.count
                    {
                     var dict = NSDictionary()
                        dict = dataDict[index] as! NSDictionary
                        if self.legislatorArray.contains(dict.value(forKey: "name_leg") as! String)
                        {   }
                        else
                        {
                            self.legislatorArray.add(dict.value(forKey: "name_leg") as! String)
                        }
                        if self.districtArray.contains(dict.value(forKey: "name_dis") as! String)
                        {   }
                        else
                        {
                            self.districtArray.add(dict.value(forKey: "name_dis") as! String)
                        }
                    }
                }catch{
                    
                }
                
        },
            failure:
            {
                (operation, error) in
                
                print(error)
                let alert = UIAlertController(
                    title: "Error",
                    message: "somthing wrong with the data",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                print("Error: " + error.localizedDescription)
        })

    }
    
    */
    
    func viewOutlet()
    {
        // view board style :
        self.view_legislator_btn.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_legislator_btn.layer.shadowOpacity = 0.5
        self.view_legislator_btn.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_legislator_btn.layer.shadowRadius = 10
        self.view_legislator_btn.layer.cornerRadius = 20
        self.view_legislator_btn.layer.masksToBounds = false
        self.view2.layer.masksToBounds = false  // view2
        self.view_legislator_btn.layer.shadowPath = UIBezierPath(rect: self.view_legislator_btn.bounds).cgPath
        self.view_legislator_btn.layer.shouldRasterize = true
        self.view_legislator_btn.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_election2018_btn.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_election2018_btn.layer.shadowOpacity = 0.5
        self.view_election2018_btn.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_election2018_btn.layer.shadowRadius = 10
        self.view_election2018_btn.layer.cornerRadius = 20
        self.view_election2018_btn.layer.masksToBounds = false
        self.view_election2018_btn.layer.shadowPath = UIBezierPath(rect: self.view_election2018_btn.bounds).cgPath
        self.view_election2018_btn.layer.shouldRasterize = true
        self.view_election2018_btn.layer.rasterizationScale =  UIScreen.main.scale
        
        self.btn_election_list1.layer.borderWidth = 1
        self.btn_election_list1.layer.cornerRadius = 5
        self.btn_election_list1.layer.borderColor = UIColor.gray.cgColor
        self.btn_election2018_list2.layer.borderWidth = 1
        self.btn_election2018_list2.layer.cornerRadius = 5
        self.btn_election2018_list2.layer.borderColor = UIColor.gray.cgColor
        self.btn_election2018_list3.layer.borderWidth = 1
        self.btn_election2018_list3.layer.cornerRadius = 5
        self.btn_election2018_list3.layer.borderColor = UIColor.gray.cgColor
        
        self.btn_legislator_list.layer.cornerRadius = 5
        self.btn_legislator_list.layer.borderWidth = 1
        self.btn_legislator_list.layer.borderColor = UIColor.gray.cgColor
        
        self.view_mainMenu_newUser.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_mainMenu_NewUser_thanks.layer.shadowOpacity = 1
        self.view_mainMenu_NewUser_thanks.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_mainMenu_NewUser_thanks.layer.shadowRadius = 10
        self.view_mainMenu_NewUser_thanks.layer.cornerRadius = 20
        self.view_mainMenu_NewUser_thanks.layer.masksToBounds = false
        self.view_mainMenu_NewUser_thanks.layer.shadowPath = UIBezierPath(rect: self.view_mainMenu_NewUser_thanks.bounds).cgPath
        self.view_mainMenu_NewUser_thanks.layer.shouldRasterize = true
        self.view_mainMenu_NewUser_thanks.layer.rasterizationScale =  UIScreen.main.scale
        
        // view board style :
        self.subView_newUser_userName.layer.borderWidth = 0.6
        self.subView_newUser_userName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_firstName.layer.borderWidth = 0.6
        self.subView_newUser_firstName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_lastName.layer.borderWidth = 0.6
        self.subView_newUser_lastName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_email.layer.borderWidth = 0.6
        self.subView_newUser_email.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_password.layer.borderWidth = 0.6
        self.subView_newUser_password.layer.borderColor = UIColor.lightGray.cgColor
        
        self.view_mainMenu_newUser.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_mainMenu_newUser.layer.shadowOpacity = 0.5
        self.view_mainMenu_newUser.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_mainMenu_newUser.layer.shadowRadius = 10
        self.view_mainMenu_newUser.layer.cornerRadius = 20
        self.view_mainMenu_newUser.layer.masksToBounds = false
        self.view_mainMenu_newUser.layer.shadowPath = UIBezierPath(rect: self.view_mainMenu_newUser.bounds).cgPath
        self.view_mainMenu_newUser.layer.shouldRasterize = true
        self.view_mainMenu_newUser.layer.rasterizationScale =  UIScreen.main.scale
        
        // view board style :
        self.view_main_menu.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_main_menu.layer.shadowOpacity = 0.5
        self.view_main_menu.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_main_menu.layer.shadowRadius = 10
        self.view_main_menu.layer.cornerRadius = 20
        self.view_main_menu.layer.masksToBounds = false
        self.view_main_menu.layer.shadowPath = UIBezierPath(rect: self.view_main_menu.bounds).cgPath
        self.view_main_menu.layer.shouldRasterize = true
        self.view_main_menu.layer.rasterizationScale =  UIScreen.main.scale
        
        // view board style :
        self.view_mainMenu_aboutUs.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_mainMenu_aboutUs.layer.shadowOpacity = 0.5
        self.view_mainMenu_aboutUs.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_mainMenu_aboutUs.layer.shadowRadius = 10
        self.view_mainMenu_aboutUs.layer.cornerRadius = 20
        self.view_mainMenu_aboutUs.layer.masksToBounds = false
        self.view_mainMenu_aboutUs.layer.shadowPath = UIBezierPath(rect: self.view_mainMenu_aboutUs.bounds).cgPath
        self.view_mainMenu_aboutUs.layer.shouldRasterize = true
        self.view_mainMenu_aboutUs.layer.rasterizationScale =  UIScreen.main.scale
        
        // view board style :
        self.view_mainMenu_ContactUS.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_mainMenu_ContactUS.layer.shadowOpacity = 0.5
        self.view_mainMenu_ContactUS.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_mainMenu_ContactUS.layer.shadowRadius = 10
        self.view_mainMenu_ContactUS.layer.cornerRadius = 20
        self.view_mainMenu_ContactUS.layer.masksToBounds = false
        self.view_mainMenu_ContactUS.layer.shadowPath = UIBezierPath(rect: self.view_mainMenu_ContactUS.bounds).cgPath
        self.view_mainMenu_ContactUS.layer.shouldRasterize = true
        self.view_mainMenu_ContactUS.layer.rasterizationScale =  UIScreen.main.scale
        
        let padding = UIView(frame: CGRect(x: 0, y: 0, width:15, height: 50))          //UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        self.txtView_mainMenu_contact_msg.placeholderText = "Your Message"
        self.txt_mainMenu_contacts_name.leftView = padding
        self.txt_mainMenu_contact_email.leftView = padding
        self.txt_mainMenu_contacts_name.layer.borderWidth = 0.6
        self.txt_mainMenu_contacts_name.layer.borderColor = UIColor.lightGray.cgColor
        self.txt_mainMenu_contact_email.layer.borderWidth = 0.6
        self.txt_mainMenu_contact_email.layer.borderColor = UIColor.lightGray.cgColor
        
        // view board style :
        self.subView_mainmenu_userSignIn_userName.layer.borderWidth = 0.6
        self.subView_mainmenu_userSignIn_userName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_mainMenu_userSignIn_email.layer.borderWidth = 0.6
        self.subView_mainMenu_userSignIn_email.layer.borderColor = UIColor.lightGray.cgColor
        
        self.view_mainMenu_UserSignIn.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_mainMenu_UserSignIn.layer.shadowOpacity = 0.5
        self.view_mainMenu_UserSignIn.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_mainMenu_UserSignIn.layer.shadowRadius = 10
        self.view_mainMenu_UserSignIn.layer.cornerRadius = 20
        self.view_mainMenu_UserSignIn.layer.masksToBounds = false
        self.view_mainMenu_UserSignIn.layer.shadowPath = UIBezierPath(rect: self.view_mainMenu_UserSignIn.bounds).cgPath
        self.view_mainMenu_UserSignIn.layer.shouldRasterize = true
        self.view_mainMenu_UserSignIn.layer.rasterizationScale =  UIScreen.main.scale
        
        self.subView_email.layer.borderWidth = 0.6
        self.subView_email.layer.borderColor = UIColor.lightGray.cgColor
        self.view_forgetPassword.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_forgetPassword.layer.shadowOpacity = 0.5
        self.view_forgetPassword.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_forgetPassword.layer.shadowRadius = 10
        self.view_forgetPassword.layer.cornerRadius = 20
        self.view_forgetPassword.layer.masksToBounds = false
        self.view_forgetPassword.layer.shadowPath = UIBezierPath(rect: self.view_forgetPassword.bounds).cgPath
        self.view_forgetPassword.layer.shouldRasterize = true
        self.view_forgetPassword.layer.rasterizationScale =  UIScreen.main.scale
        

    }
    
}


